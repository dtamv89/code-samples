/* Login and sign up processes */
(function($, window, document, undefined) {
    //Login
    $(document).on('click', '#login button[type="submit"]', function (e) {
        e.preventDefault();
        var formData = $(document).find('form').serialize();
        var url = $(document).find('form').attr('action');
        var urlDashboard = '/dashboard#setup';
        var urlAdmin = '/admin#users';

        $.post(url, formData, function (response) {
            var token = response.token;
            var role = response.role;
            if (!token) {
                noty({type: 'error', text: 'Token is missing!', timeout: 3000, layout: 'top'});
            }
            localStorage.setItem('token', token);
            if(role == 'administrator') {
                window.location = urlAdmin;
            } else {
                window.location = urlDashboard;
            }
        }).error(function (response) {
            var error = response.responseJSON;
            noty({type: 'error', text: error.error, timeout: 3000, layout: 'top'});
        });
    });

    //Sign up
    $(document).on('click', '#signUp button[type="submit"]', function (e) {
        e.preventDefault();
        var formData = $(document).find('form').serialize();
        var url = $(document).find('form').attr('action');

        $.post(url, formData, function (response) {
            $(document).find('.container-full-height-md').parent().html(response);
        }).error(function (response) {
            var errors = response.responseJSON.errors;
            $.each(errors, function(index, error){
                noty({type: 'error', text: error, timeout: 3000, layout: 'top'});
            });
        });
    });

    //Restore password
    $(document).on('click', '#restorePassword', function (e) {
        e.preventDefault();
        var email = $(document).find('#restorePasswordModal #restoreEmail').val();
        var url = $(document).find('#restorePasswordModal').data('url');

        $.post(url, {restoreEmail: email}, function (response) {
            var success = response.responseJSON;
            noty({type: 'error', text: success.message, timeout: 3000, layout: 'top'});
        }).error(function (response) {
            var error = response.responseJSON;
            noty({type: 'error', text: error.message, timeout: 3000, layout: 'top'});
        });
    });
})(jQuery, window, document);