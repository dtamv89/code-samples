<?php

namespace App\Repositories;

use App\Models\EarlySubscriber;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class UserRepository extends AbstractRepository
{
    private const EARLY_SUBSCRIBERS_TABLE = 'early_subscribers';

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function findOneByEmail(string $email): ?User
    {
        return $this->model->where('email', $email)->first();
    }

    public function getByVerifyToken(string $token): ?User
    {
        return $this->model
            ->where('verify_token', $token)
            ->whereNull('email_verified_at')
            ->first();
    }

    public function findOneByVerifyToken(string $token): ?User
    {
        return $this->model
            ->where('verify_token', $token)
            ->first();
    }

    public function findOneByUsername(string $username): ?User
    {
        return $this->model->where('username', $username)->first();
    }

    public function findOneByInviteCode(string $inviteCode): ?User
    {
        return $this->model->where('invite_code', $inviteCode)->first();
    }

    public function findEarlySubscriber(string $email): ?EarlySubscriber
    {
        return EarlySubscriber::where('email', $email)
            ->whereNull('registered_at')
            ->first();
    }

    public function registerEarlySubscriber(string $email): void
    {
        DB::table(self::EARLY_SUBSCRIBERS_TABLE)->where('email', $email)
            ->whereNull('registered_at')
            ->update(['registered_at' => Carbon::now()]);
    }

    public function findAllWithAvatar(int $limit = 100): Collection
    {
        return User::whereNotNull('file_id')->orderByDesc('created_at')->limit($limit)->get();
    }
}

