<?php

use TradingDeskApi\Models\AdUnit;
use TradingDeskApi\Models\Advertiser;
use TradingDeskApi\Models\Campaign;
use TradingDeskApi\Models\Constants\CampaignConstants;
use TradingDeskApi\Models\Constants\ExtensionsConstants;
use TradingDeskApi\Models\Constants\LineItemConstants;
use TradingDeskApi\Models\LineItem;
use TradingDeskApi\Test\DbTestCase;
use TradingDeskApi\Test\TestVariables;
use TradingDeskApi\Models\App;
use TradingDeskApi\Models\Publisher;
use TradingDeskApi\Models\Vadn;
use Illuminate\Http\Response;
use TradingDeskApi\Models\Asset;
use TradingDeskApi\Models\IabCategory;

/**
 * @group integration
 */
class AppController_IntegrationTest extends DbTestCase
{
    private $url = 'api/v2/apps';

    public function testForbiddenForAdvertiser()
    {
        $app = factory(App::class)->create();

        $url = $this->url . '/' . $app->id;

        // create method is not covered due to specific business logic

        //try to update app as advertiser
        $this->putJson($url, [], $this->getAuthHeaders($this->getAdvertiserToken()))
            ->assertStatus(Response::HTTP_FORBIDDEN);

        //try to delete app as advertiser
        $this->delete($url, [], $this->getAuthHeaders($this->getAdvertiserToken()))
            ->assertStatus(Response::HTTP_FORBIDDEN);

        //try to unarchive app as advertiser
        $this->putJson($url . '/unarchive', [], $this->getAuthHeaders($this->getAdvertiserToken()))
            ->assertStatus(Response::HTTP_FORBIDDEN);

        //try to pause app as advertiser
        $this->putJson($url . '/pause', [], $this->getAuthHeaders($this->getAdvertiserToken()))
            ->assertStatus(Response::HTTP_FORBIDDEN);

        //try to resume app as advertiser
        $this->putJson($url . '/resume', [], $this->getAuthHeaders($this->getAdvertiserToken()))
            ->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testGetAppByPublisher()
    {
        $publisherToken = $this->getPublisherToken();
        $publisher = $this->getPublisherForAuth();

        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id
        ]);

        $url = $this->url . '/' . $app->id;

        // get as publisher
        $this->getJson($url, $this->getAuthHeaders($publisherToken))
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(['name', 'description', 'vr', 'logo_url'])
            ->assertJson([
                'name' => $app->name,
                'description' => $app->description,
                'vr' => $app->vr
            ]);
    }

    public function testGetAppByAdmin()
    {
        $publisher = $this->getPublisherForAuth();

        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id
        ]);

        $url = $this->url . '/' . $app->id;

        // get as vadn admin
        $this->getJson($url, $this->getAuthHeaders($this->getAdminToken()))
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(['name', 'description', 'vr', 'logo_url'])
            ->assertJson([
                'name' => $app->name,
                'description' => $app->description,
                'vr' => $app->vr
            ]);
    }

    public function testGetAppByVadnAdmin()
    {
        $publisher = $this->getPublisherForAuth();

        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id
        ]);

        $url = $this->url . '/' . $app->id;

        // get as vadn admin
        $this->getJson($url, $this->getAuthHeaders($this->getVadnAdminToken()))
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(['name', 'description', 'vr', 'logo_url'])
            ->assertJson([
                'name' => $app->name,
                'description' => $app->description,
                'vr' => $app->vr
            ]);
    }

    public function testGetAppByAdvertiser()
    {
        $advertiserToken = $this->getAdvertiserToken();
        $publisher = $this->getPublisherForAuth();

        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id
        ]);

        $url = $this->url . '/' . $app->id;

        // get as advertiser
        $this->getJson($url, $this->getAuthHeaders($advertiserToken))
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure(['name', 'description', 'vr', 'logo_url'])
            ->assertJson([
                'name' => $app->name,
                'description' => $app->description,
                'vr' => $app->vr
            ]);


        // try to get app as advertiser - app belongs to publisher from another vadn
        $vadn = factory(Vadn::class)->create();
        $newPublisher = factory(Publisher::class)->create([
            'vadn_id' => $vadn->id
        ]);

        $app2 = factory(App::class)->create([
            'publisher_id' => $newPublisher->id
        ]);

        $url2 = $this->url . '/' . $app2->id;

        // get as advertiser
        $this->getJson($url2, $this->getAuthHeaders($advertiserToken))
            ->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testGetAppNotFound()
    {
        $url = $this->url . '/1024';

        $this->getJson($url, $this->getAuthHeaders($this->getAdminToken()))
            ->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testCreateAppByPublisher()
    {
        $url = $this->url;
        $publisher = $this->getPublisherForAuth();

        $wrongData = TestVariables::$newAppData;


        $iabCategoryParent = factory(IabCategory::class)->create();
        $iabCategoryChild = factory(IabCategory::class)->create([
            'parent_id' => $iabCategoryParent->id
        ]);

        $data = TestVariables::$newAppData;
        $data['iab_category_id'] = $iabCategoryChild->id;

        // check that validation works in case when IAB category is a child
        $publisherToken = $this->getPublisherToken();
        $this->postJson($url, $data, $this->getAuthHeaders($publisherToken))
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure([
                'errors' => [
                    'iab_category_id'
                ]
            ]);


        $data = TestVariables::$newAppData;
        $data['iab_category_id'] = $iabCategoryParent->id;

        $fileContent = Storage::disk('testsData')->get('google.png');
        $data['logo'] = base64_encode($fileContent);

        // create app as publisher
        $response = $this->postJson($url, $data, $this->getAuthHeaders($publisherToken))
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure([
                'id'
            ]);

        $createdAppId = $response->decodeResponseJson()['id'];

        $createdApp = App::find($createdAppId);
        $this->assertNotEmpty($createdApp);
        $this->assertEquals(TestVariables::$newAppData['name'], $createdApp->name);
        $this->assertEquals($publisher->id, $createdApp->publisher_id);
        $this->assertNotEmpty($createdApp->logo_path);
    }

    public function testCreateAppByVadnAdmin()
    {
        $url = $this->url;
        $vadnAdmin = $this->getVadnAdmin();
        $publisher = $this->getPublisherForAuth();
        $vadnAdminToken = $this->getVadnAdminToken();

        $wrongData = TestVariables::$newAppData;

        $iabCategoryParent = factory(IabCategory::class)->create();
        $iabCategoryChild = factory(IabCategory::class)->create([
            'parent_id' => $iabCategoryParent->id
        ]);

        $data = TestVariables::$newAppData;
        $data['iab_category_id'] = $iabCategoryChild->id;

        // check that validation works in case when IAB category is a child
        $this->postJson($url, $data, $this->getAuthHeaders($vadnAdminToken))
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure([
                'errors' => [
                    'iab_category_id'
                ]
            ]);

        // create app as vadn admin
        $data = TestVariables::$newAppData;
        $data['publisher_id'] = $publisher->id;
        $data['iab_category_id'] = $iabCategoryParent->id;

        $fileContent = Storage::disk('testsData')->get('google.png');
        $data['logo'] = base64_encode($fileContent);

        $response = $this->postJson($url, $data, $this->getAuthHeaders($vadnAdminToken))
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure([
                'id'
            ]);

        $createdAppId = $response->decodeResponseJson()['id'];

        $createdApp = App::find($createdAppId);
        $this->assertNotEmpty($createdApp);
        $this->assertEquals(TestVariables::$newAppData['name'], $createdApp->name);
        $this->assertEquals($publisher->id, $createdApp->publisher_id);
        $this->assertNotEmpty($createdApp->logo_path);

        // create app as vadn admin failed
        $publisher2 = factory(Publisher::class)->create([
            'vadn_id' => factory(Vadn::class)->create()->id
        ]);
        $data = TestVariables::$newAppData;
        $data['publisher_id'] = $publisher2->id;
        $data['iab_category_id'] = $iabCategoryParent->id;

        $this->postJson($url, $data, $this->getAuthHeaders($vadnAdminToken))
            ->assertStatus(Response::HTTP_FORBIDDEN);
    }

    public function testCreateAppByAdmin()
    {
        $url = $this->url;
        $publisher = $this->getPublisherForAuth();
        $adminToken = $this->getAdminToken();

        $iabCategoryParent = factory(IabCategory::class)->create();
        $iabCategoryChild = factory(IabCategory::class)->create([
            'parent_id' => $iabCategoryParent->id
        ]);

        $data = TestVariables::$newAppData;
        $data['iab_category_id'] = $iabCategoryChild->id;

        // check that validation works in case when IAB category is a child
        $this->postJson($url, $data, $this->getAuthHeaders($adminToken))
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure([
                'errors' => [
                    'iab_category_id'
                ]
            ]);

        // check that validation works in case when publisher_id is not Publisher's
        $wrongData = TestVariables::$newAppData;
        $wrongData['publisher_id'] = $this->getAdvertiserForAuth()->id;

        $this->postJson($url, $wrongData, $this->getAuthHeaders($adminToken))
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure([
                'errors' => [
                    'publisher_id'
                ]
            ]);

        // create app as admin
        $data = TestVariables::$newAppData;
        $data['publisher_id'] = $publisher->id;
        $data['iab_category_id'] = $iabCategoryParent->id;

        $fileContent = Storage::disk('testsData')->get('google.png');
        $data['logo'] = base64_encode($fileContent);

        $response = $this->postJson($url, $data, $this->getAuthHeaders($adminToken))
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure([
                'id'
            ]);

        $createdAppId = $response->decodeResponseJson()['id'];

        $createdApp = App::find($createdAppId);
        $this->assertNotEmpty($createdApp);
        $this->assertEquals(TestVariables::$newAppData['name'], $createdApp->name);
        $this->assertEquals($publisher->id, $createdApp->publisher_id);
        $this->assertNotEmpty($createdApp->logo_path);
    }

    public function testCreateAppByAdminWithNonexistentPublisher()
    {
        $url = $this->url;

        // create app as admin
        $data = TestVariables::$newAppData;
        // Nonexistent publisher
        $data['publisher_id'] = 109901;
        // Nonexistent asset
        $data['icon_id'] = 109901;

        $response = $this->postJson($url, $data, $this->getAuthHeaders($this->getAdminToken()))
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure([
                'errors' => [
                    'publisher_id'
                ]
            ]);
    }

    public function testUpdateAppByPublisher()
    {

        $publisher = $this->getPublisherForAuth();
        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id
        ]);

        $url = $this->url . '/' . $app->id;

        $wrongData = TestVariables::$updateAppData;
        $wrongData['platform'] = 'symbian';

        // check that validation works
        $publisherToken = $this->getPublisherToken();
        $this->putJson($url, $wrongData, $this->getAuthHeaders($publisherToken))
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertJsonStructure([
                'errors' => [
                    'platform'
                ]
            ]);

        $data = TestVariables::$updateAppData;
        $fileContent = Storage::disk('testsData')->get('google.png');
        $data['logo'] = base64_encode($fileContent);

        // update app as publisher
        $response = $this->putJson($url, $data, $this->getAuthHeaders($publisherToken))
            ->assertStatus(Response::HTTP_OK);

        $updatedApp = App::find($app->id);
        $this->assertNotEmpty($updatedApp);
        $this->assertEquals(TestVariables::$updateAppData['name'], $updatedApp->name);
        $this->assertEquals(TestVariables::$updateAppData['platform'], $updatedApp->platform);
        $this->assertNotEmpty($updatedApp->logo_path);
    }

    public function testUpdateAppByVadnAdmin()
    {

        $publisher = $this->getPublisherForAuth();
        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id
        ]);

        $url = $this->url . '/' . $app->id;

        // update app as vadn admin
        $vadnAdminToken = $this->getVadnAdminToken();
        $response = $this->putJson($url, TestVariables::$updateAppData, $this->getAuthHeaders($vadnAdminToken))
            ->assertStatus(Response::HTTP_OK);

        $updatedApp = App::find($app->id);
        $this->assertNotEmpty($updatedApp);
        $this->assertEquals(TestVariables::$updateAppData['name'], $updatedApp->name);
        $this->assertEquals(TestVariables::$updateAppData['platform'], $updatedApp->platform);

        // create app as vadn admin failed
        $publisher2 = factory(Publisher::class)->create([
            'vadn_id' => factory(Vadn::class)->create()->id
        ]);
        $app2 = factory(App::class)->create([
            'publisher_id' => $publisher2->id
        ]);
        $url = $this->url . '/' . $app2->id;
        $this->putJson($url, TestVariables::$updateAppData, $this->getAuthHeaders($vadnAdminToken))
            ->assertStatus(Response::HTTP_FORBIDDEN);

    }

    public function testUpdateAppByAdmin()
    {

        $publisher = $this->getPublisherForAuth();
        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id
        ]);

        $url = $this->url . '/' . $app->id;

        // update app as vadn admin
        $adminToken = $this->getAdminToken();
        $response = $this->putJson($url, TestVariables::$updateAppData, $this->getAuthHeaders($adminToken))
            ->assertStatus(Response::HTTP_OK);

        $updatedApp = App::find($app->id);
        $this->assertNotEmpty($updatedApp);
        $this->assertEquals(TestVariables::$updateAppData['name'], $updatedApp->name);
        $this->assertEquals(TestVariables::$updateAppData['platform'], $updatedApp->platform);

        // create app as vadn admin failed
        $publisher2 = factory(Publisher::class)->create([
            'vadn_id' => factory(Vadn::class)->create()->id
        ]);
        $app2 = factory(App::class)->create([
            'publisher_id' => $publisher2->id
        ]);
        $url = $this->url . '/' . $app2->id;
        $this->putJson($url, TestVariables::$updateAppData, $this->getAuthHeaders($adminToken))
            ->assertStatus(Response::HTTP_OK);

    }

    public function testArchiveAppByPublisher()
    {
        $publisher = $this->getPublisherForAuth();
        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id
        ]);

        $url = $this->url . '/' . $app->id;

        $this->delete($url, [], $this->getAuthHeaders($this->getPublisherToken()))
            ->assertStatus(Response::HTTP_NO_CONTENT);

        $updatedApp = App::find($app->id);
        $this->assertEquals(true, $updatedApp->archived);
    }

    public function testArchiveAppByVadnAdmin()
    {
        $publisher = $this->getPublisherForAuth();
        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id
        ]);

        $url = $this->url . '/' . $app->id;

        $this->delete($url, [], $this->getAuthHeaders($this->getVadnAdminToken()))
            ->assertStatus(Response::HTTP_NO_CONTENT);

        $updatedApp = App::find($app->id);
        $this->assertEquals(true, $updatedApp->archived);
    }

    public function testArchiveAppByAdmin()
    {
        $publisher = $this->getPublisherForAuth();
        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id
        ]);

        $url = $this->url . '/' . $app->id;

        $this->delete($url, [], $this->getAuthHeaders($this->getAdminToken()))
            ->assertStatus(Response::HTTP_NO_CONTENT);

        $updatedApp = App::find($app->id);
        $this->assertEquals(true, $updatedApp->archived);

        $this->delete($url, [], $this->getAuthHeaders($this->getAdminToken()))
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(['general' => ['Done already']]);
    }

    public function testUnarchiveAppByPublisher()
    {
        $publisher = $this->getPublisherForAuth();
        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id,
            'archived' => true
        ]);

        $url = $this->url . '/' . $app->id . '/unarchive';

        $this->put($url, [], $this->getAuthHeaders($this->getPublisherToken()))
            ->assertStatus(Response::HTTP_OK);

        $updatedApp = App::find($app->id);
        $this->assertEquals(false, $updatedApp->archived);
    }

    public function testUnarchiveAppByVadnAdmin()
    {
        $publisher = $this->getPublisherForAuth();
        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id,
            'archived' => true
        ]);

        $url = $this->url . '/' . $app->id . '/unarchive';

        $this->put($url, [], $this->getAuthHeaders($this->getVadnAdminToken()))
            ->assertStatus(Response::HTTP_OK);

        $updatedApp = App::find($app->id);
        $this->assertEquals(false, $updatedApp->archived);
    }

    public function testUnarchiveAppByAdmin()
    {
        $publisher = $this->getPublisherForAuth();
        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id,
            'archived' => true
        ]);

        $url = $this->url . '/' . $app->id . '/unarchive';

        $this->put($url, [], $this->getAuthHeaders($this->getAdminToken()))
            ->assertStatus(Response::HTTP_OK);

        $updatedApp = App::find($app->id);
        $this->assertEquals(false, $updatedApp->archived);

        $this->put($url, [], $this->getAuthHeaders($this->getAdminToken()))
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(['general' => ['Done already']]);
    }

    public function testPauseAppByPublisher()
    {
        $publisher = $this->getPublisherForAuth();
        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id
        ]);

        $url = $this->url . '/' . $app->id . '/pause';

        $this->put($url, [], $this->getAuthHeaders($this->getPublisherToken()))
            ->assertStatus(Response::HTTP_OK);

        $updatedApp = App::find($app->id);
        $this->assertEquals(true, $updatedApp->paused);
    }

    public function testPauseAppByVadnAdmin()
    {
        $publisher = $this->getPublisherForAuth();
        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id
        ]);

        $url = $this->url . '/' . $app->id . '/pause';

        $this->put($url, [], $this->getAuthHeaders($this->getVadnAdminToken()))
            ->assertStatus(Response::HTTP_OK);

        $updatedApp = App::find($app->id);
        $this->assertEquals(true, $updatedApp->paused);
    }

    public function testPauseAppByAdmin()
    {
        $publisher = $this->getPublisherForAuth();
        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id
        ]);

        $url = $this->url . '/' . $app->id . '/pause';

        $this->put($url, [], $this->getAuthHeaders($this->getAdminToken()))
            ->assertStatus(Response::HTTP_OK);

        $updatedApp = App::find($app->id);
        $this->assertEquals(true, $updatedApp->paused);

        $this->put($url, [], $this->getAuthHeaders($this->getAdminToken()))
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(['general' => ['Done already']]);
    }

    public function testPauseAppAndCampaignsByAdmin()
    {
        $publisher = $this->getPublisherForAuth();
        $advertiser = factory(Advertiser::class)->create([
            'vadn_id' => $publisher->vadn_id
        ]);

        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id
        ]);
        $adUnit = factory(AdUnit::class)->create([
            'app_id' => $app->id
        ]);

        $campaign = factory(Campaign::class)->create([
            'advertiser_id' => $advertiser->id,
        ]);

        $lineItem = factory(LineItem::class)->create([
            'status' => LineItemConstants::STATUS_RUNNING
        ]);
        $lineItem->adUnits()->attach($adUnit->id);
        $lineItem->campaign()->associate($campaign);
        $lineItem->saveOrFail();

        $url = $this->url . '/' . $app->id . '/pause';

        $this->put($url, [], $this->getAuthHeaders($this->getAdminToken()))
            ->assertStatus(Response::HTTP_OK);

        $updatedApp = App::find($app->id);
        $this->assertEquals(true, $updatedApp->paused);

        $updatedLineItem = LineItem::find($lineItem->id);
        $this->assertEquals(LineItemConstants::STATUS_PAUSED, $updatedLineItem->status);
    }

    public function testResumeAppByPublisher()
    {
        $publisher = $this->getPublisherForAuth();
        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id,
            'paused' => true
        ]);

        $url = $this->url . '/' . $app->id . '/resume';

        $this->put($url, [], $this->getAuthHeaders($this->getPublisherToken()))
            ->assertStatus(Response::HTTP_OK);

        $updatedApp = App::find($app->id);
        $this->assertEquals(false, $updatedApp->paused);
    }

    public function testResumeAppByVadnAdmin()
    {
        $publisher = $this->getPublisherForAuth();
        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id,
            'paused' => true
        ]);

        $url = $this->url . '/' . $app->id . '/resume';

        $this->put($url, [], $this->getAuthHeaders($this->getVadnAdminToken()))
            ->assertStatus(Response::HTTP_OK);

        $updatedApp = App::find($app->id);
        $this->assertEquals(false, $updatedApp->paused);
    }

    public function testResumeAppByAdmin()
    {
        $publisher = $this->getPublisherForAuth();
        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id,
            'paused' => true
        ]);

        $url = $this->url . '/' . $app->id . '/resume';

        $this->put($url, [], $this->getAuthHeaders($this->getAdminToken()))
            ->assertStatus(Response::HTTP_OK);

        $updatedApp = App::find($app->id);
        $this->assertEquals(false, $updatedApp->paused);

        $this->put($url, [], $this->getAuthHeaders($this->getAdminToken()))
            ->assertStatus(Response::HTTP_OK)
            ->assertJson(['general' => ['Done already']]);
    }

    public function testGetAllAppsByPublisher()
    {
        $basicUrl = $this->url;

        $numberOfAppsForPubisher1 = 5;
        $ids = [];

        $publisher = $this->getPublisherForAuth();
        $app = factory(App::class, $numberOfAppsForPubisher1)->create([
            'publisher_id' => $publisher->id
        ])->each(function ($a) use (&$ids) {
            $ids[] = $a->id;
        });

        $numberOfAppsForPubisher2 = 3;
        $publisher2 = factory(Publisher::class)->create([
            'vadn_id' => factory(Vadn::class)->create()->id
        ]);

        $app = factory(App::class, $numberOfAppsForPubisher2)->create([
            'publisher_id' => $publisher2->id
        ]);

        $publisher1Token = $this->getPublisherToken();
        // get all apps
        $response = $this->get($basicUrl, $this->getAuthHeaders($publisher1Token))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals($numberOfAppsForPubisher1, count($data));

        // get publishers with limit
        $url = $basicUrl . '?limit=2';
        $response = $this->get($url, $this->getAuthHeaders($publisher1Token))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals(2, count($data));

        // get publishers with offset
        $url = $basicUrl . '?offset=3';
        $response = $this->get($url, $this->getAuthHeaders($publisher1Token))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals(2, count($data));

        // get publishers with offset and limit
        $url = $basicUrl . '?offset=1&limit=10';
        $response = $this->get($url, $this->getAuthHeaders($publisher1Token))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals(4, count($data));

        // get publishers by ids
        shuffle($ids);
        $idsToGet = implode(',', array_slice($ids, 0, 2));

        $url = $basicUrl . '?filter_id=' . $idsToGet;
        $response = $this->get($url, $this->getAuthHeaders($publisher1Token))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals(2, count($data));
    }

    public function testGetAllAppsByAdmin()
    {
        $basicUrl = $this->url;

        $numberOfAppsForPubisher1 = 5;
        $ids = [];

        $publisher = $this->getPublisherForAuth();
        $app = factory(App::class, $numberOfAppsForPubisher1)->create([
            'publisher_id' => $publisher->id
        ])->each(function ($a) use (&$ids) {
            $ids[] = $a->id;
        });

        $numberOfAppsForPubisher2 = 3;
        $publisher2 = factory(Publisher::class)->create([
            'vadn_id' => factory(Vadn::class)->create()->id
        ]);

        $app = factory(App::class, $numberOfAppsForPubisher2)->create([
            'publisher_id' => $publisher2->id
        ])->each(function ($a) use (&$ids) {
            $ids[] = $a->id;
        });

        $adminToken = $this->getAdminToken();
        // get all apps
        $response = $this->get($basicUrl, $this->getAuthHeaders($adminToken))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals($numberOfAppsForPubisher1 + $numberOfAppsForPubisher2, count($data));

        // get publishers with limit
        $url = $basicUrl . '?limit=2';
        $response = $this->get($url, $this->getAuthHeaders($adminToken))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals(2, count($data));

        // get publishers with offset
        $url = $basicUrl . '?offset=3';
        $response = $this->get($url, $this->getAuthHeaders($adminToken))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals($numberOfAppsForPubisher1 + $numberOfAppsForPubisher2 - 3, count($data));

        // get publishers with offset and limit
        $url = $basicUrl . '?offset=1&limit=10';
        $response = $this->get($url, $this->getAuthHeaders($adminToken))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals(7, count($data));

        // get publishers by ids
        shuffle($ids);
        $idsToGet = implode(',', array_slice($ids, 0, 4));

        $url = $basicUrl . '?filter_id=' . $idsToGet;
        $response = $this->get($url, $this->getAuthHeaders($adminToken))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals(4, count($data));
    }

    public function testGetAllAppsByVadnAdmin()
    {
        $basicUrl = $this->url;

        $numberOfAppsForPubisher1 = 5;
        $ids = [];

        $publisher = $this->getPublisherForAuth();
        $app = factory(App::class, $numberOfAppsForPubisher1)->create([
            'publisher_id' => $publisher->id
        ])->each(function ($a) use (&$ids) {
            $ids[] = $a->id;
        });

        $numberOfAppsForPubisher2 = 3;
        $publisher2 = factory(Publisher::class)->create([
            'vadn_id' => factory(Vadn::class)->create()->id
        ]);

        $app = factory(App::class, $numberOfAppsForPubisher2)->create([
            'publisher_id' => $publisher2->id
        ]);

        $vadnAdminToken = $this->getVadnAdminToken();
        // get all apps
        $response = $this->get($basicUrl, $this->getAuthHeaders($vadnAdminToken))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals($numberOfAppsForPubisher1, count($data));

        // get publishers with limit
        $url = $basicUrl . '?limit=2';
        $response = $this->get($url, $this->getAuthHeaders($vadnAdminToken))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals(2, count($data));

        // get publishers with offset
        $url = $basicUrl . '?offset=3';
        $response = $this->get($url, $this->getAuthHeaders($vadnAdminToken))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals(2, count($data));

        // get publishers with offset and limit
        $url = $basicUrl . '?offset=1&limit=10';
        $response = $this->get($url, $this->getAuthHeaders($vadnAdminToken))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals(4, count($data));

        // get publishers by ids
        shuffle($ids);
        $idsToGet = implode(',', array_slice($ids, 0, 3));

        $url = $basicUrl . '?filter_id=' . $idsToGet;
        $response = $this->get($url, $this->getAuthHeaders($vadnAdminToken))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals(3, count($data));
    }

    public function testGetAllAppsByAdvertiser()
    {
        $basicUrl = $this->url;

        $numberOfAppsForPubisher1 = 5;
        $ids = [];

        $advertiser = $this->getAdvertiserForAuth();

        $publisher = factory(Publisher::class)->create([
            'vadn_id' => $advertiser->vadn_id
        ]);

        $apps = factory(App::class, $numberOfAppsForPubisher1)->create([
            'publisher_id' => $publisher->id
        ])->each(function ($a) use (&$ids) {
            $ids[] = $a->id;
        });

        $numberOfAppsForPubisher2 = 3;
        $publisher2 = factory(Publisher::class)->create([
            'vadn_id' => factory(Vadn::class)->create()->id
        ]);

        $apps = factory(App::class, $numberOfAppsForPubisher2)->create([
            'publisher_id' => $publisher2->id
        ]);

        $advertiserToken = $this->getAdvertiserToken();
        // get all apps
        $response = $this->get($basicUrl, $this->getAuthHeaders($advertiserToken))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals($numberOfAppsForPubisher1, count($data));

        // get publishers with limit
        $url = $basicUrl . '?limit=2';
        $response = $this->get($url, $this->getAuthHeaders($advertiserToken))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals(2, count($data));

        // get publishers with offset
        $url = $basicUrl . '?offset=3';
        $response = $this->get($url, $this->getAuthHeaders($advertiserToken))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals(2, count($data));

        // get publishers with offset and limit
        $url = $basicUrl . '?offset=1&limit=10';
        $response = $this->get($url, $this->getAuthHeaders($advertiserToken))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals(4, count($data));

        // get publishers by ids
        shuffle($ids);
        $idsToGet = implode(',', array_slice($ids, 0, 3));

        $url = $basicUrl . '?filter_id=' . $idsToGet;
        $response = $this->get($url, $this->getAuthHeaders($advertiserToken))
            ->assertStatus(Response::HTTP_OK);

        $data = $response->decodeResponseJson();
        $data = $data['data'];
        $this->assertEquals(3, count($data));
    }

    public function testSetExtensionOnCreateFailed()
    {
        $url = $this->url;
        $vadn = factory(Vadn::class)->create([
            'extension' => [
                ExtensionsConstants::NAMESPACE_ALLOWED_EXTENSIONS => [
                    [
                        'entity' => 'apps',
                        'namespaces' => [
                            'namespace_shown'
                        ]
                    ]
                ]
            ]
        ]);

        $publisher = factory(Publisher::class)->create([
            'vadn_id' => $vadn->id,
        ]);

        $adminToken = $this->getAdminToken();

        $iabCategoryParent = factory(IabCategory::class)->create();
        $iabCategoryChild = factory(IabCategory::class)->create([
            'parent_id' => $iabCategoryParent->id
        ]);

        // create app as admin
        $data = TestVariables::$newAppData;
        $data['publisher_id'] = $publisher->id;
        $data['iab_category_id'] = $iabCategoryParent->id;
        $data['extension'] = [
            'namespace_shown' => 'shown',
            'namespace_hidden' => 'hidden'
        ];

        $response = $this->postJson($url, $data, $this->getAuthHeaders($adminToken))
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertExactJson([
                'errors' => [
                    ExtensionsConstants::EXTENSION_ATTRIBUTE => 'namespace_hidden ' . ExtensionsConstants::VALIDATION_NAMESPACE_CANT_BE_SET_MESSAGE
                ]
            ]);
    }

    public function testSetExtensionOnUpdateFailed()
    {
        $vadn = factory(Vadn::class)->create([
            'extension' => [
                ExtensionsConstants::NAMESPACE_ALLOWED_EXTENSIONS => [
                    [
                        'entity' => 'apps',
                        'namespaces' => [
                            'namespace_shown'
                        ]
                    ]
                ]
            ]
        ]);

        $publisher = factory(Publisher::class)->create([
            'vadn_id' => $vadn->id,
        ]);

        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id
        ]);

        $url = $this->url . '/' . $app->id;

        $adminToken = $this->getAdminToken();

        $iabCategoryParent = factory(IabCategory::class)->create();
        $iabCategoryChild = factory(IabCategory::class)->create([
            'parent_id' => $iabCategoryParent->id
        ]);

        // create app as admin
        $data = TestVariables::$newAppData;
        $data['iab_category_id'] = $iabCategoryParent->id;
        $data['extension'] = [
            'namespace_shown' => 'shown',
            'namespace_hidden' => 'hidden'
        ];

        $response = $this->putJson($url, $data, $this->getAuthHeaders($adminToken))
            ->assertStatus(Response::HTTP_BAD_REQUEST)
            ->assertExactJson([
                'errors' => [
                    ExtensionsConstants::EXTENSION_ATTRIBUTE => 'namespace_hidden ' . ExtensionsConstants::VALIDATION_NAMESPACE_CANT_BE_SET_MESSAGE
                ]
            ]);
    }

    public function testSetExtensionOnCreateSuccessful()
    {
        $url = $this->url;
        $vadn = factory(Vadn::class)->create([
            'extension' => [
                ExtensionsConstants::NAMESPACE_ALLOWED_EXTENSIONS => [
                    [
                        'entity' => 'apps',
                        'namespaces' => [
                            'namespace_shown'
                        ]
                    ]
                ]
            ]
        ]);

        $publisher = factory(Publisher::class)->create([
            'vadn_id' => $vadn->id,
        ]);

        $adminToken = $this->getAdminToken();

        $iabCategoryParent = factory(IabCategory::class)->create();
        $iabCategoryChild = factory(IabCategory::class)->create([
            'parent_id' => $iabCategoryParent->id
        ]);

        // create app as admin
        $data = TestVariables::$newAppData;
        $data['publisher_id'] = $publisher->id;
        $data['iab_category_id'] = $iabCategoryParent->id;
        $data['extension'] = [
            'namespace_shown' => 'shown',
        ];

        $response = $this->postJson($url, $data, $this->getAuthHeaders($adminToken))
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure(['id']);

        $createdAppId = $response->decodeResponseJson()['id'];

        $createdApp = App::find($createdAppId);
        $this->assertNotEmpty($createdApp);
        $this->assertEquals(TestVariables::$newAppData['name'], $createdApp->name);
        $this->assertEquals($publisher->id, $createdApp->publisher_id);
        $this->assertEquals([
            'namespace_shown' => 'shown',
        ], json_decode($createdApp->getOriginal('extension'), true));
    }

    public function testSetExtensionOnUpdateSuccessful()
    {
        $vadn = factory(Vadn::class)->create([
            'extension' => [
                ExtensionsConstants::NAMESPACE_ALLOWED_EXTENSIONS => [
                    [
                        'entity' => 'apps',
                        'namespaces' => [
                            'namespace_shown'
                        ]
                    ]
                ]
            ]
        ]);

        $publisher = factory(Publisher::class)->create([
            'vadn_id' => $vadn->id,
        ]);

        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id
        ]);

        $url = $this->url . '/' . $app->id;

        $adminToken = $this->getAdminToken();

        $iabCategoryParent = factory(IabCategory::class)->create();
        $iabCategoryChild = factory(IabCategory::class)->create([
            'parent_id' => $iabCategoryParent->id
        ]);

        // create app as admin
        $data = TestVariables::$newAppData;
        $data['iab_category_id'] = $iabCategoryParent->id;
        $data['extension'] = [
            'namespace_shown' => 'shown',
        ];

        $response = $this->putJson($url, $data, $this->getAuthHeaders($adminToken))
            ->assertStatus(Response::HTTP_OK);

        $createdApp = App::find($app->id);
        $this->assertNotEmpty($createdApp);
        $this->assertEquals(TestVariables::$newAppData['name'], $createdApp->name);
        $this->assertEquals($publisher->id, $createdApp->publisher_id);
        $this->assertEquals([
            'namespace_shown' => 'shown',
        ], json_decode($createdApp->getOriginal('extension'), true));
    }

    public function testGetExtension()
    {
        $vadn = factory(Vadn::class)->create([
            'extension' => [
                ExtensionsConstants::NAMESPACE_ALLOWED_EXTENSIONS => [
                    [
                        'entity' => 'apps',
                        'namespaces' => [
                            'namespace_shown'
                        ]
                    ]
                ]
            ]
        ]);

        $publisher = factory(Publisher::class)->create([
            'vadn_id' => $vadn->id,
        ]);

        $app = factory(App::class)->create([
            'publisher_id' => $publisher->id,
            'extension' => [
                'namespace_hidden' => [
                    'hidden data'
                ],
                'namespace_shown' => [
                    'shown data'
                ]
            ]
        ]);

        $url = $this->url . '/' . $app->id;

        $response = $this->getJson($url, $this->getAuthHeaders($this->getAdminToken()))
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonFragment([
                'extension' => [
                    'namespace_shown' => [
                        'shown data'
                    ]
                ]
            ]);
    }
}