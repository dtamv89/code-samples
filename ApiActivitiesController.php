<?php

namespace Erunner\ApiBundle\Controller;

use Doctrine\MongoDB\LoggableCursor;
use Einder\Has\LeadBundle\Document\Activity;
use Einder\Has\LeadBundle\Document\ChanceStep;
use Einder\Has\LeadBundle\Document\Lead;
use Einder\Has\LeadBundle\Event\ChanceStepEvent;
use FOS\RestBundle\Controller\FOSRestController;
use Guzzle\Service\Exception\ValidationException;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Einder\Has\LeadBundle\Repository\Document\LeadRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/api")
 */
class ApiActivitiesController extends AbstractApiController implements ApiEnabledController
{

    /**
     * Get all activities for a user between dateFrom and DateTo
     *
     * @Route("/user/{userId}/activities/{dateFrom}/{dateTo}/{completed}", requirements={}, defaults={})
     * @Method({"GET"})
     * @Rest\View
     *
     * @ApiDoc(
     *  description="List activities connected to user in given period",
     *  section="Todo",
     *  requirements = {
     *      {
     *          "name"="userId",
     *          "dataType"="string",
     *          "requirement"="",
     *          "description"="user id"
     *      },
     *      {
     *          "name"="dateFrom",
     *          "dataType"="date",
     *          "requirement"="Y-m-d",
     *          "description"="start date"
     *      },
     *      {
     *          "name"="dateTo",
     *          "dataType"="date",
     *          "requirement"="Y-m-d",
     *          "description"="end date"
     *      },
     *  }
     * )
     */
    public function getActivitiesAction($userId, $dateFrom = null, $dateTo = null, $completed = false)
    {
        $allowedUsers = $this->get('einder_has.user_provider')->getAvaliableUsersInSessionApi();

        if (is_null($userId)) {
            $requestedUsers = [$this->getUser()->getId()];
        } else {
            $requestedUsers = [$userId];
        }

        foreach ($requestedUsers as $requestedUser) {
            if (!isset($allowedUsers[$requestedUser])) {
                throw new AccessDeniedHttpException('User has no access to this user');
            }
        }

        $user = $this->get('einder_has.user_provider')->getById($userId);

        $dateFrom = isset($dateFrom) ? \DateTime::createFromFormat('Y-m-d', $dateFrom)->modify('midnight') : (new \DateTime('midnight'));
        $dateTo = isset($dateTo) ? \DateTime::createFromFormat('Y-m-d', $dateTo)->modify('tomorrow') : (new \DateTime('tomorrow'));

        $documentManager = $this->get('doctrine_mongodb.odm.document_manager');
        $activities = $documentManager->getRepository('EinderHasLeadBundle:Activity')->getAllByDateRangeAndUsers($dateFrom, $dateTo, [$user->getId()], $completed);

        if ($error = $this->isEmpty($activities, 'There is no activities')) {
            return $error;
        }

        $res = $this->get('serializer')->serialize($activities->toArray(), 'json');

        return new Response($res);
    }

    /**
     * Get all predefined activities
     *
     * @Route("/activities/predefined", name="api_predefined_activities")
     * @Method({"GET"})
     * @Rest\View
     *
     *
     * @ApiDoc(
     *  description="Get all predefined activities",
     *  section="Todo"
     * )
     */
    public function getPredefinedActivitiesAction()
    {
        $documentManager = $this->get('doctrine_mongodb.odm.document_manager');
        $predefinedActivities = $documentManager->getRepository('EinderHasLeadBundle:PredefinedActivity')->getAll();

        if ($error = $this->isEmpty($predefinedActivities, 'There is no predefined activities')) {
            return $error;
        }

        $response = $this->get('serializer')->serialize($predefinedActivities->toArray(), 'json', SerializationContext::create()->setGroups(array('activity_details')));

        return new Response($response);
    }

    /**
     * @Route("/activities/{id}/wholeday/{value}", requirements={}, defaults={})
     * @Method({"PUT"})
     * @Rest\View
     *
     * @ApiDoc(
     *  description="Update activity's whole day",
     *  section="Todo",
     *  requirements = {
     *      {
     *          "name"="id",
     *          "dataType"="string",
     *          "requirement"="",
     *          "description"="Activity id"
     *      },
     *      {
     *          "name"="value",
     *          "dataType"="boolean",
     *          "requirement"="",
     *          "description"="Whole day value"
     *      },
     *  }
     * )
     */
    public function updateActivityWholeDayAction($id, $value)
    {
        $activity = $this->getActivity($id);
        $documentManager = $this->get('doctrine_mongodb.odm.document_manager');
        $activity->setWholeDay((bool)$value);

        $documentManager->flush();

        $response = $this->get('serializer')->serialize(['status' => 'success'], 'json');

        return new Response($response);
    }

    /**
     * @Route("/activities/{id}/repeat/{value}", requirements={}, defaults={})
     * @Method({"PUT"})
     * @Rest\View
     *
     * @ApiDoc(
     *  description="Update activity's repeat",
     *  section="Todo",
     *  requirements = {
     *      {
     *          "name"="id",
     *          "dataType"="string",
     *          "requirement"="",
     *          "description"="Activity id"
     *      },
     *      {
     *          "name"="value",
     *          "dataType"="string",
     *          "requirement"="WEEK, MONTH, YEAR",
     *          "description"="Repeat value"
     *      },
     *  }
     * )
     */
    public function updateActivityRepeatAction($id, $value)
    {
        $activityRepeatAllowedValues = array(Activity::REPEATED_EACH_WEEK, Activity::REPEATED_EACH_MONTH, Activity::REPEATED_EACH_YEAR);

        if (!in_array($value, $activityRepeatAllowedValues)) {
            throw new ValidationException('Repeat value is wrong.');
        }

        $activity = $this->getActivity($id);
        $documentManager = $this->get('doctrine_mongodb.odm.document_manager');
        $activity->setRepeat($value);

        $documentManager->flush();

        $response = $this->get('serializer')->serialize(['status' => 'success'], 'json');

        return new Response($response);
    }

    /**
     * @param $id
     * @return bool|Response
     */
    private function getActivity($id)
    {
        $user = $this->getUser();
        if ($error = $this->isNull($user, 'There is no such user')) {
            return $error;
        }

        $activity = $this->get('einder_has_lead.activity_repository')->find($id);

        if (!isset($activity)) {
            throw new NotFoundHttpException('Activity doesn\'t exist.');
        }

        return $activity;
    }

    public function jsonErrorResponse($error)
    {
        $serializer = SerializerBuilder::create()->build();
        $res = $serializer->serialize($error, 'json');

        return new Response($res);
    }
}