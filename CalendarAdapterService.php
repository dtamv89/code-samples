<?php

namespace Einder\Has\LeadBundle\Service;

use Einder\Has\LeadBundle\Document\Activity;
use Einder\Has\LeadBundle\Document\ChanceStep;
use Einder\Has\LeadBundle\ICal\MailerService;
use Einder\Has\UserBundle\Document\User;

class CalendarAdapterService
{
    /**
     * @var HASCalendarService
     */
    protected $hasCalendarService;

    /**
     * ExternalCalendarAdapterInterface
     */
    protected $externalCalendarService;

    /**
     * @var MailerService
     */
    protected $mailerService;


    /**
     * @param HASCalendarService $hasCalendarService
     * @param CalendarAdapterFactoryService $calendarAdapterFactoryService
     * @param MailerService $mailerService
     */
    function __construct(HASCalendarService $hasCalendarService, CalendarAdapterFactoryService $calendarAdapterFactoryService, MailerService $mailerService)
    {
        $this->hasCalendarService = $hasCalendarService;
        $this->externalCalendarService = $calendarAdapterFactoryService->initService();
        $this->mailerService = $mailerService;
    }

    /**
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param $requestedUsers
     * @param User $user
     * @return array
     */
    public function getEvents(\DateTime $startDate, \DateTime $endDate, $requestedUsers, User $user)
    {
        $events = [];
        $events = $this->getMergedEvents($this->getExternalCalendarEvents($startDate, $endDate, $user), $this->getHASCalendarEvents($startDate, $endDate, $requestedUsers, $user));

        return $events;
    }

    /**
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param User $user
     * @return array
     */
    private function getExternalCalendarEvents(\DateTime $startDate, \DateTime $endDate, User $user)
    {
        if (!isset($this->externalCalendarService)) {
            return [];
        }

        return $this->externalCalendarService->getEvents($startDate, $endDate, $user);
    }

    /**
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param $requestedUsers
     * @param User $user
     * @return array
     */
    private function getHASCalendarEvents(\DateTime $startDate, \DateTime $endDate, $requestedUsers, User $user)
    {
        return $this->hasCalendarService->getEvents($startDate, $endDate, $requestedUsers, $user);
    }

    /**
     * @param $externalEvents
     * @param $hasEvents
     * @return array
     */
    private function getMergedEvents($externalEvents, $hasEvents)
    {
        foreach ($externalEvents as $externalEvent) {
            if ($this->externalCalendarService instanceof EWSCalendarService) {
                $exchangeId = $externalEvent->getExchangeId();
                $hasEvents = $this->handleAllEventsInCaseOfEWS($exchangeId, $hasEvents);
            } elseif ($this->externalCalendarService instanceof GoogleCalendarService) {
                $googleId = $externalEvent->getGoogleId();
                $hasEvents = $this->handleAllEventsInCaseOfGoogle($googleId, $hasEvents);
            }
        }

        return array_merge($externalEvents, $hasEvents);
    }

    /**
     * @param $exchangeId
     * @param $hasEvents
     * @return mixed
     */
    private function handleAllEventsInCaseOfEWS($exchangeId, $hasEvents)
    {
        foreach ($hasEvents as $key => $hasEvent) {
            if ($hasEvent->getExchangeId() == $exchangeId) {
                unset($hasEvents[$key]);
            }
        }

        return $hasEvents;
    }

    /**
     * @param $googleId
     * @param $hasEvents
     * @return mixed
     */
    private function handleAllEventsInCaseOfGoogle($googleId, $hasEvents)
    {
        foreach ($hasEvents as $key => $hasEvent) {
            if ($hasEvent->getGoogleId() == $googleId) {
                unset($hasEvents[$key]);
            }
        }

        return $hasEvents;
    }

    /**
     * @param $object
     */
    public function createEvent($object)
    {
        $this->externalCalendarService->createEvent($object);
    }

    /**
     * @param $object
     */
    public function updateEvent($object)
    {
        $this->externalCalendarService->updateEvent($object);
    }

    /**
     * @param $object
     */
    public function removeEvent($object)
    {
        $this->externalCalendarService->removeEvent($object);
    }

    /**
     * @param $object
     * @param $ical
     */
    public function sendInvite($object, $ical = null)
    {
        if ($object instanceof Activity) {
            if ($this->externalCalendarService instanceof EWSCalendarService) {
                $this->externalCalendarService->sendActivityRequest($object);
                return ['type' => 'invite', 'message' => 'Event invite correctly sent to invitees'];
            }

            $reminderTime = !empty($ical['reminderTime']) ? $ical['reminderTime'] : '30';
            $agendaVisibility = !empty($ical['agendaVisibility']) ? $ical['agendaVisibility'] : null;
            $this->mailerService->sendActivityRequest($object, $reminderTime, $agendaVisibility);
            return ['type' => 'invite', 'message' => 'Event invite correctly sent to invitees'];
        }

        if ($object instanceof ChanceStep) {
            if (isset($ical['reminder']) && $ical['reminder']) {
                // Otherwise mail the invite
                $this->mailerService->sendRequests(
                    $object,
                    $object->getChanceStepEndTime(),
                    $ical['reminderTime'],
                    $ical['agendaVisibility'],
                    $ical['organizer'],
                    $ical['attendees']
                );

                return ['type' => 'invite', 'message' => 'Event invite correctly sent to invitees'];
            }
        }
    }
}
