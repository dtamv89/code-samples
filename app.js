/*global define, require, console, Globalize, React, _, $, Backbone */

//@TODO move to another file
var ncapp = {
    debug: true,
    apiBaseURL: window.noxBoxApiUri ? window.noxBoxApiUri : '/api/',

    log: function (obj) {
        'use strict';
        if (!this.isDebug()) { return; }

        console.log(obj);
    },
    logDir: function (obj) {
        'use strict';
        if (!this.isDebug()) { return; }

        console.dir(obj);
    },
    logException: function (e) {
        'use strict';
        this.logDir(e);
    },
    error: function (message) {
        'use strict';
        if (!this.isDebug()) { return; }

        throw new Error(message);
    },
    isDebug: function () {
        var isDebug = this.debug && typeof console !== 'undefined';

        return isDebug;
    },
    init: function(){
    },
    registereModel: function(name, model){
        'use strict';

        this.registeredModels.add({id: name, model: model});
    },
    getModel: function(key){
        'use strict';
        var models = this.registeredModels.where({'id': key});

        if (models.length){
            return models[0].get('model');
        } else {
            throw new Error('Model with a name ' + key + ' not found.');
        }

        return false;
    },
    registereCollection: function(name, collection){
        'use strict';

        this.registeredCollection.add({id: name, collection: collection});
    },
    getCollection: function(key){
        'use strict';

        if (!this.registeredCollection) { return; }
        var collection = this.registeredCollection.where({'id': key});

        if (collection.length){
            return collection[0].get('collection');
        } else {
            throw new Error('Model with a name ' + key + ' not found.');
        }

        return false;
    },
    i18n: function (str) {
        'use strict';
        var localizeStr = Globalize.localize(str);
        if (!localizeStr && !ncapp.I18N[str]) {
            if (ncapp.models.lang){

                ncapp.models.lang.set('langVar', str);
                ncapp.I18N[str] = true;
                ncapp.models.lang.save({silentErrors: true});
            }
        }
        return localizeStr ? localizeStr : str;
    },
    I18N: {},
    alertMessage: function (title, message, type) {
        'use strict';
        return ncapp.collections.alerts.add([{
            title: title,
            message: message,
            type: type
        }]);
    },
    em: null,
    controllers: {},
    models: {},
    collections: {},
    views: {}
};

window.ncapp = ncapp;


define([
    'jquery',
    'underscore',
    'react',
    'globalize',
    'cryptojs.sha256',
    'json!app/i18n/en.json',
    'json!app/i18n/se.json',
    'app/models/lang.model'
], function ($, _, react, globalize, CryptoJS, i18nEN, i18nSE, LangModel) {
    'use strict';
    window.React = react;
    window.CryptoJS = CryptoJS;


    require(['app/classes/initialize'], function(init){
        /* Set I18N */
        //@todo remove in production
        ncapp.models.lang = new LangModel();
        Globalize.addCultureInfo('en', {messages: i18nEN});
        Globalize.addCultureInfo('se', {messages: i18nSE});
        Globalize.culture('en');

        init.initialize();
    });

    return function(){
    };
});



