<?php

declare(strict_types=1);

namespace App\Services;

use App\Events\Subscription\SubscriptionCanceledEvent;
use App\Events\Subscription\SubscriptionCreatedEvent;
use App\Exceptions\Stripe\AlreadyHasActiveSubscriptionException;
use App\Exceptions\Stripe\HasNoActiveSubscriptionException;
use App\Exceptions\Stripe\PaymentMethodRequiredException;
use App\Exceptions\UserNotSubscribedException;
use App\Interfaces\Billable;
use App\Models\Job;
use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use Stripe\Exception\InvalidRequestException;
use Stripe\Plan;
use Stripe\Stripe;
use App\Models\Subscription;
use App\Models\ValueObjects\Invoice;
use App\Models\ValueObjects\SubscriptionDetails;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Laravel\Cashier\Invoice as CashierInvoice;
use Laravel\Cashier\InvoiceItem;

class StripeService
{
    public function __construct()
    {
        Stripe::setApiKey(Config::get('stripe.stripe_secret'));
    }

    public function createSubsctionPlan(string $planId): Plan
    {
        try {
            $plan = Plan::retrieve($planId);
        } catch (InvalidRequestException $e) {
            if (Arr::get($e->getJsonBody(), 'error.code', null) === 'resource_missing') {
                // plan doesn't not exist, so create it!

                $plan = Plan::create([
                    'id' => $planId,
                    'amount' => Config::get("stripe.{$planId}_price"),
                    'interval' => $this->getSubscriptionInterval($planId),
                    'currency' => Config::get('stripe.currency'),
                    'product' => [
                        'name' => $this->getProductName($planId)
                    ]
                ]);
            }
        }

        return $plan;
    }

    public function createSubscription(
        Billable $model,
        string $plandId,
        ?string $paymentMethod,
        bool $saveAsDefaultPaymentMethod = false
    ): Billable {
        $this->validateSubscriptionCreation($model);

        if ($model instanceof User) {
            return $this->handleUserSubscriptionCreation($model, $plandId, $paymentMethod);
        }

        if ($model instanceof Job) {
            return $this->handleJobSubscriptionCreation(
                $model,
                $plandId,
                $paymentMethod,
                $saveAsDefaultPaymentMethod
            );
        }

        throw new \Exception('Unknown Model.');
    }

    protected function handleUserSubscriptionCreation(
        User $user,
        string $plandId,
        ?string $paymentMethod
    ): Billable {
        if (!$paymentMethod && !$user->defaultPaymentMethod()) {
            throw new PaymentMethodRequiredException();
        }

        $user->newSubscription(Subscription::SUBSCRIPTION_USER, $plandId)->create($paymentMethod, [
            'email' => $user->email
        ]);

        $user->load('subscriptions');
        $this->handleSubscriptionBillingDates($user);

        event(new SubscriptionCreatedEvent($user));

        return $user;
    }

    protected function handleJobSubscriptionCreation(
        Job $job,
        string $plandId,
        ?string $paymentMethod,
        bool $saveAsDefaultPaymentMethod
    ): Billable {
        $jobOwner = $job->user;

        if (!$jobOwner->stripe_id) {
            if (!$paymentMethod) {
                throw new PaymentMethodRequiredException();
            }

            $jobOwner->createAsStripeCustomer([
                'email' => $jobOwner->email
            ]);

            $paymentMethod = $jobOwner->updateDefaultPaymentMethod($paymentMethod)->id;
        }

        if ($saveAsDefaultPaymentMethod) {
            $paymentMethod = $jobOwner->updateDefaultPaymentMethod($paymentMethod)->id;
        } elseif ($paymentMethod) {
            $paymentMethod = $jobOwner->addPaymentMethod($paymentMethod)->id;
        }

        if (!$paymentMethod && !$jobOwner->defaultPaymentMethod()) {
            throw new PaymentMethodRequiredException();
        }

        $job->newSubscription(Subscription::SUBSCRIPTION_JOB, $plandId)->create($paymentMethod, [
            'email' => $jobOwner->email
        ]);

        $job->load('subscriptions');
        $this->handleSubscriptionBillingDates($job);

        event(new SubscriptionCreatedEvent($job));

        return $job;
    }

    public function cancelSubscription(Billable $model, bool $cancelNow = false): void
    {
        if (!$model->hasActiveSubscription()) {
            throw new HasNoActiveSubscriptionException();
        }

        $subscription = $model instanceof User ? Subscription::SUBSCRIPTION_USER : Subscription::SUBSCRIPTION_JOB;

        if ($cancelNow) {
            $model->getActiveSubscription($subscription)->cancelNow();
        } else {
            $model->getActiveSubscription($subscription)->cancel();
        }

        $this->handleSubscriptionBillingDates($model);
        event(new SubscriptionCanceledEvent($model));
    }

    public function renewSubscription(Billable $model): void
    {
        if (!$model->hasActiveSubscription()) {
            throw new HasNoActiveSubscriptionException();
        }

        if (!$model->hasPaymentMethod()) {
            throw new PaymentMethodRequiredException();
        }

        $subscription = $model instanceof User ? Subscription::SUBSCRIPTION_USER : Subscription::SUBSCRIPTION_JOB;

        $model->getActiveSubscription($subscription)->resume();
        $this->handleSubscriptionBillingDates($model);
    }

    public function swapToYearlySubscription(User $user): void
    {
        if (!$user->hasActiveSubscription()) {
            throw new HasNoActiveSubscriptionException();
        }

        $user->getActiveSubscription(
            Subscription::SUBSCRIPTION_USER
        )->swap(Subscription::SUBSCRIPTION_USER_PLAN_YEARLY_ID);

        $this->handleSubscriptionBillingDates($user);
    }

    public function updatePaymentMethod(User $user, string $paymentMethod): User
    {
        if (!$user->hasActiveSubscription()) {
            throw new UserNotSubscribedException();
        }

        $user->updateDefaultPaymentMethod($paymentMethod);

        return $user;
    }

    public function getSubscriptionDetails(Billable $model): SubscriptionDetails
    {
        $subscription = $model->getActiveSubscription();
        $stripeSubscription = $subscription ? $subscription->asStripeSubscription() : null;

        $subscriptionDetails = new SubscriptionDetails();
        $subscriptionDetails->setNextBillingAt($stripeSubscription && !$stripeSubscription->cancel_at_period_end ?
            $stripeSubscription->current_period_end : null);
        $subscriptionDetails->setExpiresAt($stripeSubscription && $stripeSubscription->cancel_at_period_end ?
            $stripeSubscription->current_period_end : null);
        $subscriptionDetails->setCurrentSubscriptionPlan($subscription ? $subscription->stripe_plan : null);
        $subscriptionDetails->setIsTrial($subscription && $subscription->trial_ends_at ?
            $subscription->trial_ends_at->eq($subscription->ends_at) : false);

        return $subscriptionDetails;
    }

    public function getInvoices(User $user): Collection
    {
        $invoicesToReturn = collect();

        if (!$user->hasStripeAccount()) {
            return $invoicesToReturn;
        }

        $invoices = $user->invoices();

        $invoices->each(function (CashierInvoice $invoice) use ($invoicesToReturn) {
            $invoiceValue = new Invoice();
            $invoiceValue->setId($invoice->asStripeInvoice()->id);
            $invoiceValue->setDate($invoice->asStripeInvoice()->created);
            $invoiceValue->setAmount($invoice->total());

            // this is hacky part, for now we distinguish Job from User subscription by price
            $title = Config::get("stripe.uigiants_job_monthly_price") == $invoice->asStripeInvoice()->amount_paid
                ? Invoice::TITLE_JOB_LISTING : Invoice::TITLE_USER_PREMIUM;
            $invoiceValue->setTitle($title);

            $invoicesToReturn->push($invoiceValue);
        });

        return $invoicesToReturn->sortByDesc(function (Invoice $invoice) {
            return $invoice->getDate()->getTimestamp();
        });
    }

    public function getProductInfoForInvoice(User $user, string $invoice): array
    {
        $invoice = $user->findInvoiceOrFail($invoice);

        /** @var InvoiceItem $invoiceItem  */
        $items = $invoice->subscriptions();
        $invoiceItem = array_shift($items);

        switch ($invoiceItem->plan->id) {
            case Subscription::SUBSCRIPTION_USER_PLAN_MONTHLY_ID:
                $productName = Subscription::SUBSCRIPTION_USER_PLAN_MONTHLY_NAME;
                break;
            case Subscription::SUBSCRIPTION_USER_PLAN_YEARLY_ID:
                $productName = Subscription::SUBSCRIPTION_USER_PLAN_YEARLY_NAME;
                break;
            case Subscription::SUBSCRIPTION_JOB_PLAN_MONTHLY_ID:
                $productName = Subscription::SUBSCRIPTION_JOB_PLAN_MONTHLY_NAME;
                break;
            case Subscription::SUBSCRIPTION_JOB_PLAN_TEST_ID:
                $productName = Subscription::SUBSCRIPTION_JOB_PLAN_TEST_NAME;
                break;
            case Subscription::SUBSCRIPTION_USER_PLAN_TEST_ID:
                $productName = Subscription::SUBSCRIPTION_USER_PLAN_TEST_NAME;
                break;
        }

        return [
            'vendor' => 'UIGiants',
            'product' => $productName,
        ];
    }

    protected function validateSubscriptionCreation(Billable $model): void
    {
        $activeSubscription = $model->getActiveSubscription();

        // if no Active Subscription we can create a new one
        if (!$activeSubscription) {
            return;
        }

        throw new AlreadyHasActiveSubscriptionException();
    }

    private function getSubscriptionInterval(string $planId): string
    {
        switch ($planId) {
            case Subscription::SUBSCRIPTION_USER_PLAN_MONTHLY_ID:
            case Subscription::SUBSCRIPTION_JOB_PLAN_MONTHLY_ID:
                return Subscription::SUBSCRIPTION_PLAN_MONTHLY_INTERVAL;
            case Subscription::SUBSCRIPTION_USER_PLAN_YEARLY_ID:
                return Subscription::SUBSCRIPTION_PLAN_YEARLY_INTERVAL;
            case Subscription::SUBSCRIPTION_JOB_PLAN_TEST_ID:
            case Subscription::SUBSCRIPTION_USER_PLAN_TEST_ID:
                return Subscription::SUBSCRIPTION_PLAN_DAILY_INTERVAL;
        }

        throw new \Exception();
    }

    private function getProductName(string $planId): string
    {
        switch ($planId) {
            case Subscription::SUBSCRIPTION_USER_PLAN_MONTHLY_ID:
                return Subscription::SUBSCRIPTION_USER_PLAN_MONTHLY_NAME;
            case Subscription::SUBSCRIPTION_USER_PLAN_YEARLY_ID:
                return Subscription::SUBSCRIPTION_USER_PLAN_YEARLY_NAME;
            case Subscription::SUBSCRIPTION_JOB_PLAN_MONTHLY_ID:
                return Subscription::SUBSCRIPTION_JOB_PLAN_MONTHLY_NAME;
            case Subscription::SUBSCRIPTION_JOB_PLAN_TEST_ID:
                return Subscription::SUBSCRIPTION_JOB_PLAN_TEST_NAME;
            case Subscription::SUBSCRIPTION_USER_PLAN_TEST_ID:
                return Subscription::SUBSCRIPTION_USER_PLAN_TEST_NAME;
        }

        throw new \Exception();
    }

    public function handleSubscriptionBillingDates(Billable $model): void
    {
        $subscriptionDetails = $this->getSubscriptionDetails($model);
        $subscription = $model->getActiveSubscription();

        $subscription->next_billing_at = $subscriptionDetails->getNextBillingAt();
        $subscription->save();
    }

    /**
     * Will be deleted in future, need it for launch only
     */
    public function createFreeSubscription(User $user, int $trialPeriod): User
    {
        $user->newSubscription(Subscription::SUBSCRIPTION_USER, Subscription::SUBSCRIPTION_USER_PLAN_YEARLY_ID)
            ->trialUntil(Carbon::now()->addMonths($trialPeriod))
            ->create(null, [
                'email' => $user->email
            ]);

        $user->load('subscriptions');
        $this->handleSubscriptionBillingDates($user);

        event(new SubscriptionCreatedEvent($user));

        // we cancel it immediatly due to User can't add payment deatils to trial subscription
        // so when trial one finishes, User has to subscribe
        $this->cancelSubscription($user);

        return $user;
    }
}

