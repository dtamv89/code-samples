<?php

use TradingDeskApi\Models\Advertiser;
use TradingDeskApi\Models\User;
use TradingDeskApi\Models\VadnAdmin;
use TradingDeskApi\Models\Constants\UserConstants;
use TradingDeskApi\Services\AdvertiserService;
use TradingDeskApi\Exceptions\{
    TradingDeskValidationException, TradingDeskNotFoundException
};
use TradingDeskApi\Models\Constants\AdvertiserConstants;
use TradingDeskApi\Models\AdvertiserProfile;
use TradingDeskApi\Models\Vadn;
use TradingDeskApi\Managers\{
    AdvertiserManager, VadnManager
};
use Illuminate\Support\Collection;

/**
 * @group unit
 * @runTestsInSeparateProcesses
 */
class AdvertiserServiceTest extends TestCase
{
    public function testGetAdvertiserWithProfileSuccessfully()
    {
        $advertiser = new Advertiser(['vadn_id' => 1]);
        $advertiserManager = Mockery::mock(AdvertiserManager::class);
        $advertiserManager->shouldReceive('find')->once()->withAnyArgs()->andReturn($advertiser);
        App::instance(AdvertiserManager::class, $advertiserManager);

        $servicePartialMock = Mockery::mock(AdvertiserService::class . '[getAsArrayWithMergedProfile]');
        $servicePartialMock->shouldReceive('getAsArrayWithMergedProfile')->once()->andReturn($advertiser->toArray());

        $result = $servicePartialMock->getWithProfile(1);

        $this->assertNotEmpty($result);
    }


    public function testGetAdvertiserFails()
    {
        $advertiser = new Advertiser(['vadn_id' => 1]);
        $advertiserManager = Mockery::mock(AdvertiserManager::class);
        $advertiserManager->shouldReceive('find')->once()->withAnyArgs()->andThrow(TradingDeskNotFoundException::class);
        App::instance(AdvertiserManager::class, $advertiserManager);

        $servicePartialMock = Mockery::mock(AdvertiserService::class . '[getAsArrayWithMergedProfile]');
        $servicePartialMock->shouldReceive('getAsArrayWithMergedProfile')->never()->andReturn($advertiser->toArray());

        $this->expectException(TradingDeskNotFoundException::class);
        $servicePartialMock->getWithProfile(1);
    }

    public function testCreateAdvertiserSuccessfully()
    {
        DB::shouldReceive('transaction')
            ->once()
            ->andReturn(1);

        $servicePartialMock = Mockery::mock(AdvertiserService::class . '[validateData]');
        $servicePartialMock->shouldReceive('validateData')->once()->andReturn(true);

        $result = $servicePartialMock->create(['publisher_id' => 1]);
        $this->assertEquals(1, $result);
    }

    public function testCreateAdvertiserFails()
    {
        DB::shouldReceive('transaction')
            ->never()
            ->andReturn(1);

        $servicePartialMock = Mockery::mock(AdvertiserService::class . '[validateData]');
        $servicePartialMock->shouldReceive('validateData')->once()->andThrow(TradingDeskValidationException::class);

        $this->expectException(TradingDeskValidationException::class);
        $servicePartialMock->create(['publisher_id' => 1]);
    }

    public function testUpdateAdvertiserSuccessfully()
    {
        $advertiser = new Advertiser(['vadn_id' => 1]);
        $advertiser->profile = new AdvertiserProfile();

        $advertiserManager = Mockery::mock(AdvertiserManager::class);
        $advertiserManager->shouldReceive('find')->once()->withAnyArgs()->andReturn($advertiser);
        App::instance(AdvertiserManager::class, $advertiserManager);

        DB::shouldReceive('transaction')
            ->once()
            ->andReturn(1);

        $servicePartialMock = Mockery::mock(AdvertiserService::class . '[validateData]');
        $servicePartialMock->shouldReceive('validateData')->once()->andReturn(true);

        $result = $servicePartialMock->update(['advertiser_id' => 1], 1, 'admin_update');
        $this->assertEquals(1, $result);
    }

    public function testUpdateAdvertiserFails()
    {
        $advertiser = new Advertiser(['vadn_id' => 1]);
        $advertiserManager = Mockery::mock(AdvertiserManager::class);
        $advertiserManager->shouldReceive('find')->once()->withAnyArgs()->andThrow(TradingDeskNotFoundException::class);
        App::instance(AdvertiserManager::class, $advertiserManager);

        DB::shouldReceive('transaction')
            ->never()
            ->andReturn(1);

        $servicePartialMock = Mockery::mock(AdvertiserService::class . '[validateData]');

        $this->expectException(TradingDeskNotFoundException::class);
        $servicePartialMock->update(['advertiser_id' => 1], 1, 'admin_update');
    }

    public function testArchiveAdvertiserSuccessfully()
    {
        $advertiser = Mockery::mock(Advertiser::class . '[saveOrFail]');
        $advertiser->shouldReceive('saveOrFail')->once()->andReturn(true);
        $advertiserManager = Mockery::mock(AdvertiserManager::class);
        $advertiserManager->shouldReceive('find')->once()->withAnyArgs()->andReturn($advertiser);
        App::instance(AdvertiserManager::class, $advertiserManager);

        $service = new AdvertiserService();

        $result = $service->archive(1);
        $this->assertEmpty($result);
    }

    public function testArchiveAdvertiserFails()
    {
        $advertiser = Mockery::mock(Advertiser::class . '[saveOrFail]');
        $advertiser->shouldReceive('saveOrFail')->never()->andReturn(true);
        $advertiserManager = Mockery::mock(AdvertiserManager::class);
        $advertiserManager->shouldReceive('find')->once()->withAnyArgs()->andThrow(TradingDeskNotFoundException::class);
        App::instance(AdvertiserManager::class, $advertiserManager);

        $service = new AdvertiserService();

        $this->expectException(TradingDeskNotFoundException::class);
        $service->archive(1);
    }

    public function testUnarchiveAdvertiserSuccessfully()
    {
        $advertiser = Mockery::mock(Advertiser::class . '[saveOrFail]');
        $advertiser->archived = true;
        $advertiser->shouldReceive('saveOrFail')->once()->andReturn(true);
        $advertiserManager = Mockery::mock(AdvertiserManager::class);
        $advertiserManager->shouldReceive('find')->once()->withAnyArgs()->andReturn($advertiser);
        App::instance(AdvertiserManager::class, $advertiserManager);

        $service = new AdvertiserService();

        $result = $service->unarchive(1);
        $this->assertEmpty($result);
    }

    public function testUnarchiveAdvertiserFails()
    {
        $advertiser = Mockery::mock(Advertiser::class . '[saveOrFail]');
        $advertiser->shouldReceive('saveOrFail')->never()->andReturn(true);
        $advertiserManager = Mockery::mock(AdvertiserManager::class);
        $advertiserManager->shouldReceive('find')->once()->withAnyArgs()->andThrow(TradingDeskNotFoundException::class);
        App::instance(AdvertiserManager::class, $advertiserManager);

        $service = new AdvertiserService();

        $this->expectException(TradingDeskNotFoundException::class);
        $service->unarchive(1);
    }

    public function testGetCommissionSuccessfully()
    {
        $advertiser = new Advertiser();
        $advertiserProfile = new AdvertiserProfile();
        $advertiserProfile->demand_cpm_fee = 1.2;
        $advertiserProfile->demand_revshare = 10;
        $advertiser->profile = $advertiserProfile;

        $advertiserManager = Mockery::mock(AdvertiserManager::class);
        $advertiserManager->shouldReceive('find')->once()->withAnyArgs()->andReturn($advertiser);
        App::instance(AdvertiserManager::class, $advertiserManager);

        $service = new AdvertiserService();
        $result = $service->getCommissions(1);

        $expected = [
            AdvertiserConstants::DEMAND_CPM_FEE => 1.2,
            AdvertiserConstants::DEMAND_REVSHARE => 10
        ];
        $this->assertEquals($expected, $result);
    }

    public function testGetCommissionFails()
    {
        $advertiser = new Advertiser();
        $advertiserProfile = new AdvertiserProfile();
        $advertiserProfile->demand_cpm_fee = 1.2;
        $advertiserProfile->demand_revshare = 10;
        $advertiser->profile = $advertiserProfile;

        $advertiserManager = Mockery::mock(AdvertiserManager::class);
        $advertiserManager->shouldReceive('find')->once()->withAnyArgs()->andThrow(TradingDeskNotFoundException::class);
        App::instance(AdvertiserManager::class, $advertiserManager);

        $service = new AdvertiserService();

        $this->expectException(TradingDeskNotFoundException::class);
        $service->getCommissions(1);
    }

    public function testUpdateCommissionFails()
    {
        $advertiser = new Advertiser();
        $advertiser->id = 1;
        $advertiserProfile = Mockery::mock(AdvertiserProfile::class . '[update]');
        $advertiserProfile->shouldReceive('update')->andReturn(true);
        $advertiser->profile = $advertiserProfile;

        $advertiserManager = Mockery::mock(AdvertiserManager::class);
        $advertiserManager->shouldReceive('find')->once()->withAnyArgs()->andReturn($advertiser);
        App::instance(AdvertiserManager::class, $advertiserManager);

        $servicePartialMock = Mockery::mock(AdvertiserService::class . '[validateData]');
        $servicePartialMock->shouldReceive('validateData')->once()->andThrow(TradingDeskValidationException::class);

        $this->expectException(TradingDeskValidationException::class);
        $servicePartialMock->updateCommissions([], 1);
    }

    public function testUpdateCommissionSuccessfully()
    {
        $advertiser = new Advertiser();
        $advertiser->id = 1;
        $advertiserProfile = Mockery::mock(AdvertiserProfile::class . '[update]');
        $advertiserProfile->shouldReceive('update')->andReturn(true);
        $advertiser->profile = $advertiserProfile;

        $advertiserManager = Mockery::mock(AdvertiserManager::class);
        $advertiserManager->shouldReceive('find')->once()->withAnyArgs()->andReturn($advertiser);
        App::instance(AdvertiserManager::class, $advertiserManager);

        $servicePartialMock = Mockery::mock(AdvertiserService::class . '[validateData]');
        $servicePartialMock->shouldReceive('validateData')->once()->andReturn(true);

        $result = $servicePartialMock->updateCommissions([], 1);

        $this->assertEquals(1, $result);
    }

    public function testGetAllAdvertisersFromVadn()
    {
        for ($i = 0; $i < 4; $i++) {
            $advertisers[] = new User();
        }

        $advertisers = new Collection($advertisers);

        $advertiserManager = Mockery::mock(AdvertiserManager::class);
        $advertiserManager->shouldReceive('findAllWithProfilesByVadn')->twice()->withAnyArgs()->andReturn($advertisers);
        App::instance(AdvertiserManager::class, $advertiserManager);

        $vadn = new Vadn();
        $vadnManager = Mockery::mock(VadnManager::class);
        $vadnManager->shouldReceive('find')->once()->withAnyArgs()->andReturn($vadn);
        App::instance(VadnManager::class, $vadnManager);

        $service = new AdvertiserService();
        $results = $service->getAllWithProfilesByVadn([], 1);

        $this->assertNotEmpty($results);
        $this->assertEquals(4, count($results['data']));
        $this->assertEquals(4, $results['total']);
    }

    public function testGetAllAdvertisersByAdmin()
    {
        for ($i = 0; $i < 4; $i++) {
            $advertisers[] = new User();
        }

        $advertisers = new Collection($advertisers);

        $advertiserManager = Mockery::mock(AdvertiserManager::class);
        $advertiserManager->shouldReceive('findAllWithProfiles')->twice()->withAnyArgs()->andReturn($advertisers);
        App::instance(AdvertiserManager::class, $advertiserManager);

        $service = new AdvertiserService();
        $results = $service->getAllWithProfilesByAdmin([]);

        $this->assertNotEmpty($results);
        $this->assertEquals(4, count($results['data']));
        $this->assertEquals(4, $results['total']);
    }
}
