<?php

//LOGIN TO APP
$I = new AjaxWebTester($scenario);
$I->wantTo('Create a new todo with the todobar');
$I->amOnPage('/logout');
$I->amOnPage('/');
$I->seeCurrentUrlEquals('/app_test.php/login');
$I->see('Login');
$I->fillField('_username', 'admin@erunner.eu');
$I->fillField('_password', 'tijdelijk');
$I->click('_submit');
$I->seeCurrentUrlEquals('/app_test.php/todo');

$I->see('Admin Admin');

// Create a test account
$I->amOnPage('/lead/new');
$I->see('ADD NEW ACCOUNT');

$I->fillField('#has_lead_add_new_lead_name', 'todotest');
$I->fillField('#has_lead_add_new_lead_phone', '12345');
$I->fillField('#has_lead_add_new_lead_street', 'abc straat');
$I->fillField('#has_lead_add_new_lead_number', '1');
$I->fillField('#has_lead_add_new_lead_postal_code', '1234');
$I->fillField('#has_lead_add_new_lead_city', 'Amsterdam');
$I->fillField('#has_lead_add_new_lead_country', 'Nederland');
$I->click('button');
$I->see('12345');
$I->see('Amsterdam');

//CHECK IF THIS IS ACCOUNT VIEW
$I->see('Selling activities');

// Check the todo page
$I->amOnPage('/todo');
$I->see('Yesterday');
$I->see('Earlier');
$I->see('Today');
$I->see('Tomorrow');
$I->see('Add');

// Check the todo bar
$I->fillField('#todo-bar-add-input', 'Add a new test todo');
$I->click('Add');
$I->waitForElementVisible('.todo-header');
$I->waitForText('Add a new test todo', null, '.todo-column-today');

// Sidebar should load now with newly created todo
$I->click('Add a new test todo');
$I->waitForElementVisible("#side-bar");
$I->see('Other');
$I->see('Make this todo an opportunity');#
$I->fillField('#todoNameInput', 'testtodo');
$I->makeScreenshot('aftertesttod filled in');
$I->waitForElementVisible('#todoSaveButton');
sleep(1);
$I->makeScreenshot('aftertesttod filled in waiting for save button');
$I->click("Save");

// @todo For some reason this doesn't work while running in phantomjs

$I->makeScreenshot('After save has been clicked');
// Sidebar should disappear, notification shown,  testtodo should show up
$I->waitForElementVisible('.noty_message');

$I->makeScreenshot('After notification has been shown');
// This seems needed somehow as otherwise the text isn't found, while for normal use this does work. @todo check why this does not work
//$I->reloadPage();

$I->makeScreenshot('After reload');

//$I->waitForText('testtodo');
$I->see('testtodo');

$I->fillField('#todo-bar-add-input', '@to');
$I->waitForElementVisible('.mentions-autocomplete-list');
$I->waitForText('todotest');
$I->see('todotest'); // Autocomplete should be visible
$I->click('.mentions-autocomplete-list li');
$I->pressKey('#todo-bar-add-input', WebDriverKeys::ENTER);

$I->see('todotest');
$I->click('todotest');
$I->see('todotest');
$I->see('Amsterdam');

// Logout again
$I->amOnPage('/logout');