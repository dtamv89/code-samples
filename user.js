/*global module*/
'use strict';

var Backbone = require('backbone');
var _ = require('underscore');
var $ = require('jquery');
var EJS = require('ejs');

var template = require('../../templates/admin/user.ejs');
var templateOverview = require('../../templates/admin/user-overview.ejs');
var templateUserInfo = require('../../templates/admin/user-info.ejs');
var templatePayments = require('../../templates/admin/user-payments.ejs');

var User = require('../../models/user.model.js');
var PaymentsCollection = require('../../collections/payments.collection.js');

var UserAdminView = Backbone.View.extend({

    el: '#page-wrapper',
    elTabContent: '.tab-content',
    userId: null,
    model: null,
    activeTab: 'overviewTab',
    paymentCollection: null,
    tabs: [
        {
            key: 'overviewTab',
            action: 'fetchPayments'
        },
        {
            key: 'userInfoTab',
            action: 'renderUserInfo'
        },
        {
            key: 'paymentsTab',
            action: 'fetchPayments'
        }

    ],

    events:{
        'change input' : 'userUpdate',
        'click #overviewTab, #userInfoTab, #paymentsTab' : 'switchTab',
        'click #saveUser': 'saveUser',
        'click #cancel': 'cancelAction'
    },

    initialize: function (options) {
        this.userId = options.userId;
        this.model = new User({id: this.userId});
        this.model.fetch();
        this.model.on('sync', this.render, this);
    },

    userUpdate: function(e) {
        var target = e.currentTarget,
            data = {};
        data[target.name] = target.value;
        this.model.set(data);
    },

    render:function () {
        $(this.el).html(EJS.render(template({user: this.model.toJSON()})));
        this.fetchPayments();
        return this;
    },

    renderOverview: function () {
        $(this.elTabContent).html(EJS.render(templateOverview({
            user: this.model.toJSON(),
            payments: this.paymentCollection.toJSON()
        })));
        return this;
    },

    renderUserInfo: function () {
        $(this.elTabContent).html(EJS.render(templateUserInfo({user: this.model.toJSON()})));
        return this;
    },

    renderPayments: function () {
        $(this.elTabContent).html(EJS.render(templatePayments({
            payments: this.paymentCollection.toJSON(),
            user: this.model.toJSON()
        })));
        return this;
    },

    fetchPayments: function () {
        this.paymentCollection = new PaymentsCollection({id: this.userId});
        this.paymentCollection.bind('reset', this.render);
        this.paymentCollection.fetch();

        if(this.activeTab == 'overviewTab') {
            this.paymentCollection.on('sync', this.renderOverview, this);
            return this;
        }
        this.paymentCollection.on('sync', this.renderPayments, this);
    },

    switchTab: function (e) {
        var tabName = $(e.currentTarget).attr('id');
        if (this.activeTab == tabName) {
            return this;
        }
        this.activeTab = tabName;
        var tabObject = _.first(_.where(this.tabs, {key: tabName}));

        //here we call tab switch related function
        this[tabObject.action]();
    },

    saveUser: function() {
        this.model.save();
    },

    cancelAction: function () {
        window.history.back();
    }
});

module.exports = UserAdminView;