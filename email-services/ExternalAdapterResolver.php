<?php

namespace App\Resolvers;

use App\Adapters\ConstantContactAdapter;
use App\Adapters\ConvertKitAdapter;
use App\Adapters\KlaviyoAdapter;
use App\Adapters\MailchimpAdapter;
use App\Contracts\DataSyncAdapterContract;
use App\Contracts\EspAdapterContract;
use App\Exceptions\InvalidExternalServiceException;
use App\Service\Models\ExternalService;
use App\Team\Models\Team;
use App\Team\Models\TeamExternalService;
use GuzzleHttp\Client;

/**
 * Class ExternalAdapterResolver.
 */
class ExternalAdapterResolver
{
    /**
     * @param Team $team
     * @return EspAdapterContract|null
     * @throws InvalidExternalServiceException
     * @throws \App\Exceptions\MailchimpMetadataException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getEspAdapter(Team $team): ?EspAdapterContract
    {
        $teamExternalService = $team->external_esp;

        // this part stands for SparkPost
        if ($teamExternalService === null) {
            return null;
        }

        switch ($teamExternalService->external_service->slug) {
            case ExternalService::MAILCHIMP_SLUG:
                return new MailchimpAdapter($teamExternalService);
            case ExternalService::KLAVIYO_SLUG:
                return new KlaviyoAdapter($teamExternalService);
            case ExternalService::CONSTANTCONTACT_SLUG:
                return new ConstantContactAdapter($teamExternalService);
            default:
                throw new InvalidExternalServiceException();
        }
    }

    /**
     * @param TeamExternalService $teamExternalService
     * @return DataSyncAdapterContract
     * @throws InvalidExternalServiceException
     * @throws \App\Exceptions\MailchimpMetadataException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getDataSyncAdapter(TeamExternalService $teamExternalService): DataSyncAdapterContract
    {
        switch ($teamExternalService->external_service->slug) {
            case ExternalService::MAILCHIMP_SLUG:
                return new MailchimpAdapter($teamExternalService);
            case ExternalService::KLAVIYO_SLUG:
                return new KlaviyoAdapter($teamExternalService);
            case ExternalService::CONSTANTCONTACT_SLUG:
                return new ConstantContactAdapter($teamExternalService);
            case ExternalService::CONVERTKIT_SLUG:
                return new ConvertKitAdapter($teamExternalService, new Client());
            default:
                throw new InvalidExternalServiceException();
        }
    }
}

