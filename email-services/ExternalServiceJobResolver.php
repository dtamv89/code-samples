<?php

namespace App\Resolvers;

use App\Exceptions\InvalidExternalServiceException;
use App\Jobs\ConstantContact\ConstantContactListImportJob;
use App\Jobs\ConvertKit\ConvertKitListImportJob;
use App\Jobs\Klaviyo\KlaviyoListImportJob;
use App\Jobs\Mailchimp\MailChimpListExportJob;
use App\Jobs\Mailchimp\MailChimpListImportJob;
use App\Service\Models\ExternalService;
use App\Team\Models\TeamExternalService;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class ExternalServiceJobResolver.
 */
class ExternalServiceJobResolver
{
    /**
     * @param TeamExternalService $teamExternalService
     * @return ShouldQueue
     * @throws InvalidExternalServiceException
     */
    public function getListJob(TeamExternalService $teamExternalService): ShouldQueue
    {
        switch ($teamExternalService->external_service->slug) {
            case ExternalService::MAILCHIMP_SLUG:
                return new MailChimpListImportJob($teamExternalService);
            case ExternalService::KLAVIYO_SLUG:
                return new KlaviyoListImportJob($teamExternalService);
            case ExternalService::CONSTANTCONTACT_SLUG:
                return new ConstantContactListImportJob($teamExternalService);
            case ExternalService::CONVERTKIT_SLUG:
                return new ConvertKitListImportJob($teamExternalService);
            default:
                throw new InvalidExternalServiceException();
        }
    }

    /**
     * @param TeamExternalService $teamExternalService
     * @return ShouldQueue
     * @throws InvalidExternalServiceException
     */
    public function getTwoWaySyncJob(TeamExternalService $teamExternalService): ShouldQueue
    {
        switch ($teamExternalService->external_service->slug) {
            case ExternalService::MAILCHIMP_SLUG:
                return new MailChimpListExportJob($teamExternalService);
            default:
                throw new InvalidExternalServiceException();
        }
    }
}

