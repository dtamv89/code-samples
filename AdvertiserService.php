<?php

namespace TradingDeskApi\Services;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Auth\User;
use TradingDeskApi\Exceptions\TradingDeskAuthorizationException;
use TradingDeskApi\Exceptions\TradingDeskValidationException;
use TradingDeskApi\Managers\VadnManager;
use TradingDeskApi\Models\Account;
use TradingDeskApi\Models\Advertiser;
use TradingDeskApi\Models\AdvertiserProfile;
use DB;
use TradingDeskApi\Models\Constants\AdvertiserConstants;
use TradingDeskApi\Traits\ResponseTrait;
use TradingDeskApi\Traits\UserTrait;
use TradingDeskApi\Traits\ValidationTrait;
use TradingDeskApi\Models\Constants\UserConstants;
use Auth;
use App;
use TradingDeskApi\Models\SearchCriteria;
use TradingDeskApi\Managers\AdvertiserManager;
use TradingDeskApi\Exceptions\TradingDeskDoneAlreadyException;

/**
 * Class AdvertiserService
 * @package TradingDeskApi\Services
 */
class AdvertiserService
{
    use ValidationTrait, UserTrait, ResponseTrait;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var AccountService
     */
    protected $accountService;

    /**
     * @var AdvertiserManager
     */
    protected $advertiserManager;

    /**
     * @var VadnManager
     */
    protected $vadnManager;

    /**
     * AdvertiserService constructor.
     */
    public function __construct()
    {
        $this->userService = App::make(UserService::class);
        $this->accountService = App::make(AccountService::class);
        $this->advertiserManager = App::make(AdvertiserManager::class);
        $this->vadnManager = App::make(VadnManager::class);
    }

    /**
     * @param $advertiserId
     * @return Advertiser
     */
    public function get($advertiserId)
    {
        return $this->advertiserManager->find($advertiserId);
    }

    /**
     * @param null $advertiserId
     * @param bool $forAdmin
     * @return mixed
     */
    public function getWithProfile($advertiserId = null, $forAdmin = false)
    {
        $advertiser = $this->get($advertiserId);

        /** Only Admin can see vadn_id attribute */
        if ($forAdmin) {
            $advertiser->makeVisible('vadn_id');
        }

        return $this->getAsArrayWithMergedProfile($advertiser);
    }

    /**
     * @param array $params
     * @param $vadnId
     * @return array
     */
    public function getAllWithProfilesByVadn(array $params, $vadnId)
    {
        $searchCriteria = new SearchCriteria($params);
        $vadn = $this->vadnManager->find($vadnId);

        $data = $this->advertiserManager->findAllWithProfilesByVadn($searchCriteria, $vadn);
        $total = $this->advertiserManager->findAllWithProfilesByVadn($searchCriteria, $vadn, true)->count();

        return $this->handleResponseForList($data, $total);
    }

    /**
     * @param array $params
     * @return array
     */
    public function getAllWithProfilesByAdmin(array $params)
    {
        $searchCriteria = new SearchCriteria($params);

        $data = $this->advertiserManager->findAllWithProfiles($searchCriteria);

        /** Only Admin can see vadn_id attribute */
        $data->each(function ($item) {
            $item->makeVisible('vadn_id');
        });

        $total = $this->advertiserManager->findAllWithProfiles($searchCriteria, true)->count();

        return $this->handleResponseForList($data, $total);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        $this->validateAdvertiserCreate($data);

        return DB::transaction(function () use ($data) {
            $advertiser = Advertiser::create($data);

            $advertiserProfile = new AdvertiserProfile($data);
            $advertiserProfile->advertiser()->associate($advertiser);
            $advertiserProfile->saveOrFail();

            $this->accountService->create($advertiser->id);

            return $advertiser->id;
        });
    }

    /**
     * @param $data
     * @param $advertiserId
     * @param $scenario
     * @return mixed
     * @throws TradingDeskAuthorizationException
     */
    public function update($data, $advertiserId, $scenario)
    {
        $advertiser = $this->get($advertiserId);
        $advertiser->setScenario($scenario);
        $advertiser->profile->setScenario(AdvertiserProfile::SCENARIO_UPDATE);
        $validationRule = $scenario;

        $this->validateAdvertiserUpdate($data, $validationRule);

        return DB::transaction(function () use ($data, $advertiser) {
            $advertiser->profile->update($data);
            $advertiser->update($data);

            return $advertiser->id;
        });
    }

    /**
     * @param $advertiserId
     * @throws TradingDeskDoneAlreadyException
     */
    public function archive($advertiserId)
    {
        $advertiser = $this->get($advertiserId);

        if ($this->userService->isUserArchived($advertiser)) {
            throw new TradingDeskDoneAlreadyException();
        }

        $this->userService->archive($advertiser);
    }

    /**
     * @param $advertiserId
     * @throws TradingDeskDoneAlreadyException
     */
    public function unarchive($advertiserId)
    {
        $advertiser = $this->get($advertiserId);

        if (!$this->userService->isUserArchived($advertiser)) {
            throw new TradingDeskDoneAlreadyException();
        }

        $this->userService->unarchive($advertiser);
    }

    /**
     * @param $advertiserId
     * @return array
     * @throws TradingDeskAuthorizationException
     */
    public function getCommissions($advertiserId)
    {
        $advertiser = $this->get($advertiserId);

        $commissions = [
            AdvertiserConstants::DEMAND_CPM_FEE => $advertiser->profile->demand_cpm_fee,
            AdvertiserConstants::DEMAND_REVSHARE => $advertiser->profile->demand_revshare
        ];

        return $commissions;
    }

    /**
     * @param $data
     * @param $advertiserId
     * @return mixed
     * @throws TradingDeskAuthorizationException
     */
    public function updateCommissions($data, $advertiserId)
    {
        $advertiser = $this->get($advertiserId);
        $this->validateCommissionsUpdate($data);

        /**
         * On update commissions we can update only demand_cpm_fee/demand_revshare
         */
        $advertiser->profile->setScenario(AdvertiserProfile::SCENARIO_COMMISSIONS);
        $advertiser->profile->update($data);

        return $advertiser->id;
    }

    /**
     * @param $data
     * @throws TradingDeskValidationException
     */
    private function validateAdvertiserCreate($data)
    {
        $validationRules = array_merge(AdvertiserProfile::$rules[AdvertiserProfile::SCENARIO_CREATE], Advertiser::$rules[Advertiser::SCENARIO_CREATE]);
        $this->validateData($data, $validationRules, Advertiser::$messages);
    }

    /**
     * @param $data
     * @param $validationRule
     * @throws TradingDeskValidationException
     */
    private function validateAdvertiserUpdate($data, $validationRule)
    {
        $validationRules = array_merge(AdvertiserProfile::$rules[AdvertiserProfile::SCENARIO_UPDATE], Advertiser::$rules[$validationRule]);
        $this->validateData($data, $validationRules, Advertiser::$messages);
    }

    /**
     * @param $data
     * @throws TradingDeskValidationException
     */
    private function validateCommissionsUpdate($data)
    {
        $this->validateData($data, AdvertiserProfile::$rules[AdvertiserProfile::SCENARIO_COMMISSIONS]);
    }
}