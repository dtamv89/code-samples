<?php

namespace Einder\Has\LeadBundle\Manager;

use Einder\Has\CoreBundle\Manager\AbstractManager;
use Einder\Has\LeadBundle\Document\Country;
use Einder\Has\LeadBundle\Document\Field;
use Einder\Has\LeadBundle\Entity\Lead;
use Einder\Has\LeadBundle\Repository\Document\FieldRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


class FieldManager extends AbstractManager
{
    /** @var FieldRepository $repository */
    protected $repository;

    /**
     * @param $contactsEnabled
     */
    public function setContactsEnabled($contactsEnabled)
    {
        $this->contactsEnabled = $contactsEnabled;
    }

    /**
     * @param $translator
     */
    public function setTranslator($translator)
    {
        $this->translator = $translator;
    }

    /**
     * Find lead fields.
     *
     * @param $on_list
     * @param array $selectedRemovableFields
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findLeadFields($on_list = null, $selectedRemovableFields = array())
    {
        return $this->findLeadFieldsWithOptions(array('onList' => $on_list), $selectedRemovableFields);
    }

    /**
     * Find lead fields with options.
     *
     * @param array $options
     * @param array $selectedRemovableFields
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findLeadFieldsWithOptions($options = array(), $selectedRemovableFields = array())
    {
        return $this->repository->getLeadFields($options, $selectedRemovableFields);
    }

    /**
     * @param null $on_list
     * @param array $selectedRemovableFields
     * @return mixed
     */
    public function findTurnoverFields($on_list = null, $selectedRemovableFields = array())
    {
        return $this->repository->getTurnoverFields($on_list, $selectedRemovableFields);
    }

    /**
     * @param null $on_list
     * @param array $selectedRemovableFields
     * @return mixed
     */
    public function findWholesalerFields($on_list = null, $selectedRemovableFields = array())
    {
        return $this->repository->getWholesalerFields($on_list, $selectedRemovableFields);
    }

    /**
     * @return mixed
     */
    public function findAccountPlanFields()
    {
        return $this->repository->getAccountPlanFields();
    }

    /**
     * @param $type
     * @return mixed
     */
    public function findRemovableFromListFields($type)
    {
        switch ($type) {
            case 'lead':
            case 'lead_list':
                return $this->repository->getRemovableFromListLeadFields();
                break;
            case 'wholesaler_list':
                return $this->repository->getRemovableFromListWholesalerFields();
                break;
            case 'contact':
                return $this->repository->getRemovableFromListContactFields();
                break;
        }
    }

    /**
     * @return array
     */
    public function findSearchableLeadFields()
    {
        $fields = $this->repository->getSearchableLeadFields();

        return $this->getLowerFieldNames($fields);
    }

    /**
     * @return array
     */
    public function findSearchableContactFields()
    {
        $fields = $this->repository->getSearchableContactFields();

        return $this->getLowerFieldNames($fields);
    }

    /**
     * @return array
     */
    public function findSearchableWholesalerFields()
    {
        $fields = $this->repository->getSearchableWholesalerFields();

        return $this->getLowerFieldNames($fields);
    }

    /**
     * @param $fields
     *
     * @return array
     */
    public function getLowerFieldNames($fields)
    {
        $result = array();
        foreach ($fields as $field) {
            $result[] = $field->getNameLower();
        }

        return $result;
    }

    /**
     * @return array
     */
    public function findSearchableStepFields()
    {
        $fields = $this->repository->getSearchableStepFields();

        $result = array();
        foreach ($fields as $field) {
            $result[] = $field->getName() . '_lower';
        }

        return $result;
    }

    /**
     * Find lead fields to import.
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findLeadFieldsToImport()
    {
        $fields = $this->findLeadFields();
        $importFields = array();
        foreach ($fields as $field) {
            $importFields[$field->getLabel()] = $field;
        }

        return $importFields;
    }

    /**
     * @return array
     */
    public function findContactFieldsToImport()
    {
        $fields = $this->findContactFields();
        $importFields = array();
        foreach ($fields as $field) {
            $importFields[$field->getLabel()] = $field;
        }

        return $importFields;
    }

    /**
     * Find lead fields.
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findLeadViewFields()
    {
        return $this->repository->getLeadViewFields();
    }

    /**
     * Find contact fields.
     *
     * @param $on_list
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findContactFields($on_list = null, $selectedRemovableFields = array())
    {
        return $this->repository->getContactFields($on_list, $selectedRemovableFields);
    }

    /**
     * Find contact fields.
     *
     * @param $on_list
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findContactFieldsLimited($on_list = null, $selectedRemovableFields = array())
    {
        return $this->repository->getContactFieldsLimited($on_list, $selectedRemovableFields);
    }

    /**
     * Find step fields.
     *
     * @param $on_list
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function findStepFields($on_list = null)
    {
        return $this->repository->getStepFields($on_list);
    }

    /**
     * Delete Lead by its id.
     *
     * @param int $id
     *
     * @throws AccessDeniedException|NotFoundHttpException
     */
    public function deleteById($id)
    {
        $field = $this->findOneById($id);

        if (!$field) {
            throw new NotFoundHttpException('There is no such field');
        }

        $this->remove($field);
    }

    /**
     * Disable field on group to avoid removing from all groups.
     *
     * @param int $id
     * @param string $type
     *
     * @throws AccessDeniedException|NotFoundHttpException
     */
    public function disableById($id, $type)
    {
        $field = $this->findOneById($id);
        if (!$field) {
            throw new NotFoundHttpException('There is no such field');
        }
        $field->disable($type);

        // If all are disabled, we can remove the field
        if ($field->allDisabled()) {
            return $this->deleteById($id);
        }

        // Other fields still active, just disable this one
        $this->save($field);
    }

    /**
     * @param $fieldName
     * @param null $collection
     * @return mixed
     */
    public function getChoicesForFieldByName($fieldName, $collection = null)
    {
        return $this->repository->getChoicesForField($fieldName, $collection);
    }

    /**
     * @param $field
     * @param null $collection
     * @return mixed
     */
    public function getChoicesForField($field, $collection = null)
    {
        if ($field->getRealType() == 'choice') {
            $choices = $field->getChoices()->toArray();
        } else {
            $choices = $this->repository->getChoicesForField($field->getDatabasePath(), $collection);
        }

        return $choices;
    }

    /**
     * @param $field
     * @param null $collection
     * @return mixed
     */
    public function getUniqueValuesNumberOfField($field, $collection = null)
    {
        if ($field->getRealType() == 'choice') {
            $choices = count($field->getChoices()->toArray());
        } else {
            $choices = $this->repository->getUniqueValuesNumberOfField($field->getDatabasePath(), $collection);
        }

        return $choices;
    }

    /**
     * @param $field
     * @param null $collection
     * @return array
     */
    public function getChoicesForStaticField($field, $collection = null)
    {
        if ($field->getName() == 'status') {
            return array('false' => 'Open', 'true' => 'Closed');
        } else {
            $choices = $this->repository->getChoicesForField($field->getDatabasePath(), $collection);
        }

        return array_combine($choices, $choices);
    }

    /**
     * @return mixed
     */
    public function findLeadFieldsForAdmin()
    {
        return $this->repository->getLeadFieldsForAdmin();
    }

    /**
     * @param bool $orderMode
     *
     * @return mixed
     */
    public function findFieldsChoices($orderMode)
    {
        return $this->repository->getFieldsChoices($orderMode);
    }

    /**
     * @return mixed
     */
    public function findContactFieldsForAdmin()
    {
        return $this->repository->getContactFieldsForAdmin();
    }

    /**
     * @return mixed
     */
    public function findStepFieldsForAdmin()
    {
        return $this->repository->getStepFieldsForAdmin();
    }

    /**
     * @return mixed
     */
    public function findWholesalerFieldsForAdmin()
    {
        return $this->repository->getWholesalerFieldsForAdmin();
    }

    /**
     * @return mixed
     */
    public function findTurnoverFieldsForAdmin()
    {
        return $this->repository->getTurnoverFieldsForAdmin();
    }

    /**
     * @return mixed
     */
    public function findAccountPlanFieldsForAdmin()
    {
        return $this->repository->getAccountPlanFieldsForAdmin();
    }

    /**
     * Find all fields by given fieldGroup and realType
     * result should be mapped to fit choice form type.
     *
     * @return array
     */
    public function findAllChoiceFieldsForSelect($realType, $fieldGroup)
    {
        $fields = $this->repository->findAllFieldsByTypeAndGroup($realType, $fieldGroup);
        $choices = array();

        foreach ($fields as $field) {
            $choices[$field->getName()] = $field->getLabel();
        }

        return $choices;
    }

    /**
     * Create a new field.
     *
     * @param $label
     *
     * @return Field
     */
    public function createLeadField($label)
    {
        // @todo get latest position and add 1 to that
        $position = 50;

        $field = $this->addField($label, $position, 'Add new lead', false, true);
        $field->setForLead(true);
        $this->objectManager->persist($field);

        return $field;
    }

    /**
     * Create a new field.
     *
     * @param $label
     *
     * @return Field
     */
    public function createContactField($label)
    {
        // @todo get latest position and add 1 to that
        $position = 50;

        $field = $this->addField($label, $position, 'Contact');
        $field->setForContact(true);
        $this->objectManager->persist($field);

        return $field;
    }

    /**
     * @param $label
     * @param $position
     * @param $heading
     * @return Field
     */
    private function addField($label, $position, $heading)
    {
        $field = new Field();
        $field->setLabel($label);
        $field->setHeading($heading);
        $field->setPosition($position);
        $field->setRealType('text');
        $field->setFixed(false);
        $field->setRequired(true);
        $field->setOnList(true);
        $field->setSearchable(true);
        $field->setFilterable(false);
        $field->setForLead(false);
        $field->setForContact(false);
        $field->setForStep(false);
        $field->setRoleForView([]);
        $field->setRoleForEdit([]);

        return $field;
    }

    /**
     * @return mixed
     */
    public function getLeadFieldsToImportLabeled()
    {
        return $this->addLabels($this->findLeadFieldsToImport(), 'Company');
    }

    /**
     * @return mixed
     */
    public function getContactFieldsToImportLabeled()
    {
        return $this->addLabels($this->findContactFieldsToImport(), 'Contact');
    }

    /**
     * @return mixed
     */
    public function getCombinedFieldForImport()
    {
        if ($this->contactsEnabled) {
            return array_merge(
                $this->getLeadFieldsToImportLabeled(),
                $this->getContactFieldsToImportLabeled()
            );
        }

        return $this->getLeadFieldsToImportLabeled();
    }

    /**
     * Add a label to each field to show what kind of field it is.
     *
     * @todo Make this url/json data proof and look ok to the user
     *
     * @param $fields
     * @param $label
     *
     * @return mixed
     */
    protected function addLabels($fields, $label)
    {
        foreach ($fields as $key => $field) {
            // If the fieldname already contains the label, don't add it
            if (strpos($field, $label) === false) {
                if (is_numeric($key)) {
                    $fields[$key] = $label . ' ' . $field;
                } else {
                    unset($fields[$key]);
                    $key = $this->translator->trans($label) . ' ' . str_replace('-', '', $this->translator->trans($key));
                    $fields[$key] = $field;
                }
            }
        }

        return $fields;
    }

    /**
     * @param $type
     *
     * @return array
     */
    public function getRequiredOptionalForType($type)
    {
        $result = $this->repository->findRequiredOptionalForType($type);
        $requiredOptionalFieldNames = array();
        foreach ($result as $field) {
            $requiredOptionalFieldNames[] = array('label' => $field->getLabel(), 'name' => $field->getName());
        }

        return $requiredOptionalFieldNames;
    }

    /**
     * @param $value
     *
     * @return mixed
     */
    public function handleValueForCountryType($value)
    {
        $country = $this->objectManager->getRepository('EinderHasLeadBundle:Country')->findOneBy(array('ISO' => $value));

        return $country->getCountryId();
    }
}
