<?php

namespace App\Repositories;

use App\Contracts\TeamRepository as Contract;
use App\Events\Team\ApprovalStatusChanged;
use App\Services\AdPlacementService;
use Domain\KarmaCredit\Models\TokenTransaction;
use Domain\Matching\Models\Matching;
use Domain\Service\Models\Connection;
use Domain\Team\Models\Question;
use Domain\Team\Models\Team;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class WindowFunctionSampleRepository
 */
class WindowFunctionSampleRepository implements Contract
{
    /**
     * This is quite complex query for getting relevant Team to be advertised
     * In this method we check following hard gate conditions:
     *  - Does the brand have any live ads(Live ad is an existing Ad inventory)? - Yes
     *  - Does the brand have enough Amp Credits to pay for the placement? - Yes
     *  - Is the brand matched? - Yes
     *  - Has this brand appeared in more than 49% of email newsletter ad placements hosted by this brand? - No
     *  - Does the brand have an Instagram connection? - Yes
     *
     * Also we check deciding factors:
     *  - The matching score between brands - Points in matchings
     *  - Has the brand boosted this month? - If Yes, +20 points
     *  - Amp credits balance vs cost of placement - If Has >200% of placement cost, +10 points
     */
    public function findOneForAdPlacement(Team $team, int $adCostEstimateInTokens): Collection
    {
        return Team::query()->fromQuery(
            "select t.*, m.points as matching_points, ap.shown_at as ad_shown_at, COALESCE(pr.appearancePercent,0) as appearance_percent,
            SUM(points + IF(tr.team_id IS NULL,0,20) + IF(t.tokens_balance > :adPlacementCostX2,10,0)) OVER (PARTITION BY t.id) as total_points,
            CASE
                WHEN t.last_shoutout_at > DATE_SUB(NOW(), INTERVAL 24 HOUR) THEN 50
                WHEN t.last_shoutout_at between DATE_SUB(NOW(), INTERVAL 48 HOUR) and DATE_SUB(NOW(), INTERVAL 24 HOUR) THEN 40
                WHEN t.last_shoutout_at between DATE_SUB(NOW(), INTERVAL 7 DAY) and DATE_SUB(NOW(), INTERVAL 48 HOUR) THEN 30
                WHEN t.last_shoutout_at between DATE_SUB(NOW(), INTERVAL 30 DAY) and DATE_SUB(NOW(), INTERVAL 7 DAY) THEN 20
                ELSE 0 
            END as dynamic_points,
            COUNT(m.team_id) over() as matchings_count
            from teams t
            join matchings m on m.team_id = t.id
            join collaboration_inventories ci on t.id = ci.team_id AND ci.published_at IS NOT NULL AND (ci.expires_at > NOW() OR ci.expires_at is NULL)
            left join (SELECT host_team_id, ad_team_id, 
                    ROUND(100 * count(ad_team_id)/SUM(count(ad_team_id)) OVER ()) as appearancePercent
                    FROM ad_placements 
                    where host_team_id = :hostTeamId1
                    group by ad_team_id ,host_team_id
                ) pr on pr.ad_team_id = t.id
            left join ad_placements ap on ap.ad_team_id = t.id AND ap.shown_at = (select MAX(shown_at) from ad_placements where host_team_id = :hostTeamId2)
            left join token_transactions tr on tr.team_id = t.id AND DATE_FORMAT(tr.created_at,'%Y-%m') = DATE_FORMAT(NOW(),'%Y-%m') AND tr.type = :transactionBoosted
            where ci.deleted_at IS NULL
                AND m.target_team_id = :hostTeamId3 AND m.status = :matchingApproved AND m.deleted_at IS NULL
                AND t.tokens_balance >= :adPlacementCost AND t.shoutouts_advertising = 1
            group by tr.team_id, t.id
            having count(ci.id) > 0
            order by dynamic_points DESC, total_points DESC",
            [
                'hostTeamId1' => $team->id,
                'hostTeamId2' => $team->id,
                'hostTeamId3' => $team->id,
                'matchingApproved' => Matching::STATUS_APPROVED,
                'adPlacementCost' => $adCostEstimateInTokens,
                'adPlacementCostX2' => $adCostEstimateInTokens * 2,
                'transactionBoosted' => TokenTransaction::TYPE_BOOSTING,
            ]
        );
    }
}
