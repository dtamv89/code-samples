<?php

namespace Project\Services;

use Illuminate\Http\Request;

use Cookiefy\Http\Requests;
use Cookiefy\Http\Controllers\Controller;
use Validator;
use Cookiefy\Models\User;
use Cookiefy\Models\CreditCard;

use Config;
use Braintree_Configuration;
use Braintree_ClientToken;
use Braintree_Transaction;
use Braintree_Customer;
use Braintree_Subscription;
use Braintree_TransactionSearch;

class BrainTreeService
{
    protected $brainTreePaymentPlanId;

    public function __construct()
    {
        $this->init();
    }

    public function init()
    {
        Braintree_Configuration::environment(config('braintree.environment'));
        Braintree_Configuration::merchantId(config('braintree.merchantId'));
        Braintree_Configuration::publicKey(config('braintree.publicKey'));
        Braintree_Configuration::privateKey(config('braintree.privateKey'));
        $this->brainTreePaymentPlanId = config('braintree.recurringPaymentPlanId');
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return Braintree_ClientToken::generate();
    }

    /**
     * @param User $user
     * @param CreditCard $creditCard
     * @return array
     */
    public function createPaymentMethod(User $user, CreditCard $creditCard)
    {
        $creditCardToken = $this->createCustomerWithCard($user, $creditCard);
        if (empty($creditCard)) {
            return ['success' => false, 'message' => 'Customer creation has failed.'];
        }

        $subscriptionId = $this->subscribeToPlan($creditCardToken, $user);
        if (empty($subscriptionId)) {
            return ['success' => false, 'message' => 'Payment method subscription has failed.'];
        }

        $user->braintree_subscription_id = $subscriptionId;
        $user->handleNextPaymentDate();
        $user->handleLastCardNumber($creditCard->card_number);
        $user->save();

        return ['success' => true, 'message' => 'Payment method has been added.'];
    }

    /**
     * @param User $user
     * @return mixed
     */
    public function getPayments(User $user)
    {
        $payments = Braintree_Transaction::search([
            Braintree_TransactionSearch::customerId()->is($user->braintree_customer_id),
        ]);

        return $payments;
    }

    /**
     * @param User $user
     * @param \DateTime $periodBegin
     * @param \DateTime $periodEnd
     * @return mixed
     */
    public function getPaymentForUserByDatePeriod(User $user, \DateTime $periodBegin, \DateTime $periodEnd)
    {
        $searchCriteria = [
            Braintree_TransactionSearch::customerId()->is($user->braintree_customer_id)
        ];

        $periodBegin = $periodBegin->format('m/d/Y h:m');
        $periodEnd = $periodEnd->format('m/d/Y h:m');
        $dateBetween = Braintree_TransactionSearch::createdAt()->between($periodBegin, $periodEnd);
        $searchCriteria[] = $dateBetween;

        $payments = Braintree_Transaction::search($searchCriteria);

        return $payments;
    }

    /**
     * @param User $user
     * @param CreditCard $creditCard
     * @return bool
     */
    private function createCustomerWithCard(User $user, CreditCard $creditCard)
    {
        $result = Braintree_Customer::create([
            'firstName' => $this->getFirstName($user->name),
            'lastName' => $this->getLastName($user->name),
            'company' => $user->company_name,
            'email' => $user->email,
            'website' => $user->website,
            'creditCard' => array(
                'cardholderName' => $creditCard->owner_name,
                'number' => $creditCard->card_number,
                'expirationMonth' => $creditCard->card_expiration_month,
                'expirationYear' => $creditCard->card_expiration_year,
                'cvv' => $creditCard->cvv,
                'billingAddress' => array(
                    'firstName' => $this->getFirstName($user->name),
                    'lastName' => $this->getLastName($user->name)
                )
            )
        ]);


        if ($result->success) {
            $user->braintree_customer_id = $result->customer->id;
            return $result->customer->creditCards[0]->token;
        }

        return false;
    }

    /**
     * @param $creditCardToken
     * @param User $user
     * @return mixed
     */
    private function subscribeToPlan($creditCardToken, User $user)
    {
        if ($user->plan == User::PLAN_BASIC) {
            $result = $this->createWithoutTrialPeriod($creditCardToken);
        } else {
            $result = $this->createWithTrialPeriod($creditCardToken, $user);
        }

        if ($result->success) {
            return $subscriptionId = $result->subscription->id;
        }
    }

    /**
     * @param $creditCardToken
     * @param User $user
     * @return mixed
     */
    private function createWithTrialPeriod($creditCardToken, User $user)
    {
        $trialDays = $user->getTrialPeriodLeft();
        $result = Braintree_Subscription::create([
            'paymentMethodToken' => $creditCardToken,
            'planId' => $this->brainTreePaymentPlanId,
            'trialDuration' => $trialDays
        ]);

        return $result;
    }

    /**
     * @param $creditCardToken
     * @return mixed
     */
    private function createWithoutTrialPeriod($creditCardToken)
    {
        $result = Braintree_Subscription::create([
            'paymentMethodToken' => $creditCardToken,
            'planId' => $this->brainTreePaymentPlanId,
            'trialPeriod' => false
        ]);

        return $result;
    }

    /**
     * @param $name
     * @return mixed
     */
    private function getFirstName($name)
    {
        $nameSplitted = explode(' ', $name);
        return $nameSplitted[0];
    }

    /**
     * @param $name
     * @return string
     */
    private function getLastName($name)
    {
        $nameSplitted = explode(' ', $name);
        return isset($nameSplitted[1]) ? $nameSplitted[1] : '';
    }
}