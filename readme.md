# Code Sample README


## Local Deploy

Run `composer install`

Create you  environment file `.env` and define there you local env postfix

Create you local environment file `.env.local`(check variables in the .env.example)

**Currently we have some issues so if you run script via console set DB_HOST=localhost in the .env.local file**

To create DB schema run `php artisan db:schema:create`

To migrate db run `php artisan migrate`

To fill `platform_settings` table run `php artisan db:seed --class=PlatformSettingsTableSeeder`

To generate AdSever valid data run `php artisan db:seed --class=DataSetForAdServerSeeder`

To generate JWT_SECRET run `php artisan td-jwt:generate`

## Migration of old DB

To migrate old db to new run `php artisan old-td-data:migrate`

After DB has been migrated all users have to update their passwords. In order to send emails to all active users run `php artisan reset-password-emails:send`

If it's necessary to delete some VADNs after migration following command should be run `php artisan vadns:delete {ids}` where ids are ids of VADNs which should stay, eg `php artisan vadns:delete 1 2 12`

## Misc data migration

To migrate all IAB categories run `php artisan iab:parse data/iab/IAB-categories.csv`
To migrate Maxmind run `php artisan maxmind:parse data/maxmind/GeoLite2-City-Locations-en.csv`

## Coding standards

Follow PHP PSR-2 coding standards

## Swagger

In order to use swagger locally run `l5-swagger:publish-assets` to publish swagger-ui to your public folder

To update Swagger execute `php artisan l5-swagger:generate`

## Tests

Create you local environment file `.env.testing`(check variables in the .env.testing.example)

Set credentials for tests in file `.env.testing`

To propagate initial data execute `php artisan db:seed --env=testing`

To run all test execute `./vendor/bin/phpunit`

To run certain test execute `./vendor/bin/phpunit --filter=AuthenticationTest`

To run group of unit tests `./vendor/bin/phpunit --group unit`

To run group of integration tests `./vendor/bin/phpunit --group integration`

# DB for tests

To create DB schema for integration test run `php artisan db:schema:create --env=testing`

All DB seeders for tests are located in the /database/seeds/tests

## Mailing

In order to have mail functionality `MAIL_*` vars should be set in the appropriate `.env*` file

## Data generation for Ad Server

To generate data for Ad Server execute next command `php artisan ad-server-data:generate`
To validate generated data execute next command `php artisan ad-server-data:validate`
All generated data is stored in `data/ad-server/data.json`. If generated data is invalid file `data.json` won't be updated.