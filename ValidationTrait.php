<?php

namespace TradingDeskApi\Traits;

use Illuminate\Foundation\Auth\User;
use TradingDeskApi\Exceptions\TradingDeskAuthorizationException;
use TradingDeskApi\Exceptions\TradingDeskValidationException;
use TradingDeskApi\Models\Vadn;
use Validator;

/**
 * Class ValidationTrait
 * @package TradingDeskApi\Traits
 */
trait ValidationTrait
{
    /**
     * @var string
     */
    private $attributeForbiddenText = 'Attribute is not supported.';

    /**
     * @SWG\Definition(
     *     definition="Error",
     *     required={ "attribute"},
     *     @SWG\Property(
     *         property="attribute",
     *         type="array",
     *        @SWG\Items()
     *     )
     * )
     *
     * @SWG\Definition(
     *         definition="ValidationError",
     *         required={ "errors"},
     *         @SWG\Property(
     *             property="errors",
     *             type="array",
     *             @SWG\Items(ref="#/definitions/Error")
     *         )
     * )
     *
     * @param $data
     * @param array $rules
     * @param array $messages
     * @throws TradingDeskValidationException
     */
    public function validateData($data, array $rules, array $messages = [])
    {
        $errors = $this->validateInputAttributes($data, $rules);
        $validator = Validator::make($data, $rules, $messages);

        if ($validator->fails()) {
            $errors = array_merge($errors, $validator->errors()->messages());
            throw new TradingDeskValidationException($errors);
        } elseif (!empty($errors)) {
            throw new TradingDeskValidationException($errors);
        }
    }

    /**
     * @param array $data
     * @param array $rules
     * @return array
     */
    private function validateInputAttributes(array $data, array $rules)
    {
        $errors = [];
        $attributesDifference = array_diff_key($data, $rules);

        if (empty($attributesDifference)) {
            return $errors;
        }

        foreach ($attributesDifference as $attribute => $value) {
            $errors[$attribute][] = $this->attributeForbiddenText;
        }

        return $errors;
    }

    /**
     * !! Only fields that are defined in model's rules are allowed !!
     * @param $data
     * @param array $allowedProperties
     * @return mixed
     */
    public function sanitizeData($data, array $allowedProperties)
    {
        foreach ($data as $key => $value) {
            if (!in_array($key, $allowedProperties)) {
                unset($data[$key]);
            }
        }

        return $data;
    }

    /**
     * @param $vadnHost
     * @param $requestHost
     * @return mixed
     */
    public function validateHost($vadnHost, $requestHost)
    {
        return strpos($vadnHost, $requestHost);
    }
}



