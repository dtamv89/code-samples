const aws = require('aws-sdk');
const s3 = new aws.S3();
const ses = new aws.SES({ region: 'us-east-1' });
const simpleParser = require('mailparser').simpleParser;
const planer = require('planer');
const https = require('https');
const crypto = require('crypto');

const bucketName = process.env.bucket;
const prefix = process.env.folder;
const secret = process.env.secret;
const host = process.env.host;

function sendPost(to, from, message) {
  const data = JSON.stringify({
    to: to,
    from: from,
    message: message
  })

  const options = {
    hostname: host,
    port: 443,
    path: '/aws/webhook/ses',
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'AWS-Signature': crypto.createHash('md5').update(to + secret + from).digest('hex')
    }
  }

  const req = https.request(options, (res) => {
    res.on('data', (d) => {
      console.log('Request was sent.')
    })
  })

  req.on('response', (res) => {
    if (res.statusCode === 200) {
      console.log('Message was sent to API.')
    } else {
      console.log('Something went wrong. Check response body.')
    }
  })

  req.on('error', (e) => {
    console.log('Problem with request: ' + e.message);
  })

  req.write(data)
  req.end()
}

exports.handleUserMessage = function (event, context, callback) {
  var sesNotification = event.Records[0].ses;
  console.log("SES Notification:\n", JSON.stringify(sesNotification, null, 2));

  // Retrieve the email from your bucket
  s3.getObject({
    Bucket: bucketName,
    Key: prefix + sesNotification.mail.messageId
  }, function (err, data) {
    if (err) {
      console.log(err, err.stack);
      callback(err);
    } else {
      // Custom email processing goes here
      simpleParser(data.Body, (err, parsed) => {
        if (err) {
          console.log(err, err.stack);
          callback(err);
        } else {
          console.log("Ref:" + parsed.references)
          let replyText = planer.extractFromPlain(parsed.text);
          sendPost(parsed.to.value[0].address, parsed.from.value[0].address, replyText);
          callback(null, null);
        }
      });

      callback(null, null);
    }
  });
};


