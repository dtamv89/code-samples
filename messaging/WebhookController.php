<?php

declare(strict_types=1);

namespace App\Http\Controllers\Aws;

use App\Http\Controllers\Controller;
use App\Http\Middleware\VerifyAwsWebhookSignature;
use App\Http\Requests\Message\SesMessageRequest;
use App\Services\MessageService;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class WebhookController extends Controller
{
    public function __construct()
    {
        $this->middleware(VerifyAwsWebhookSignature::class);
    }

    public function handleSesMessage(MessageService $messageService, SesMessageRequest $request)
    {
        try {
            $messageService->createMessageFromSes($request->validated());
        } catch (BadRequestHttpException $e) {
            return \response()->json(['error' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return \response()->json();
    }
}

