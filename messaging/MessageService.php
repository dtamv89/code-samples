<?php

declare(strict_types=1);

namespace App\Services;

use App\Events\UserNotificationEvent;
use App\Exceptions\Message\RecipientAndSenderAreTheSameException;
use App\Mail\EmailMessageMail;
use App\Repositories\MessageRepository;
use App\Models\{Message, Notification, User};
use App\Repositories\UserRepository;
use App\Services\Mail\MailService;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class MessageService
{
    private UserRepository $userRepository;
    private MailService $mailService;
    private MessageRepository $messageRepository;

    public function __construct(
        UserRepository $userRepository,
        MailService $mailService,
        MessageRepository $messageRepository
    ) {
        $this->userRepository = $userRepository;
        $this->mailService = $mailService;
        $this->messageRepository = $messageRepository;
    }

    public function createMessageFromSes(array $data): Message
    {
        $username = explode('@', $data['to']);
        $recipient = $this->userRepository->findOneByUsername($username[0]);
        $sender = $this->userRepository->findOneByEmail($data['from']);

        if (!$recipient || !$sender) {
            throw new BadRequestHttpException('Invalid recipient or/and sender.');
        }

        if ($recipient->id === $sender->id) {
            throw new BadRequestHttpException('Recipient and sender are the same.');
        }

        $message = new Message();
        $message->recipient_id = $recipient->id;
        $message->sender_id = $sender->id;
        $message->message = strip_tags($data['message']);
        $message->save();

        $messages = $this->messageRepository->findAllByRecipientAndSender($message->recipient, $message->sender);

        $this->mailService->send(new EmailMessageMail($messages, $message->sender, $message->recipient));

        return $message;
    }

    public function sendMessage(User $sender, User $recipient, string $messageText): Message
    {
        if ($sender->id === $recipient->id) {
            throw new RecipientAndSenderAreTheSameException();
        }

        $message = new Message();
        $message->recipient_id = $recipient->id;
        $message->sender_id = $sender->id;
        $message->message = strip_tags($messageText);
        $message->save();

        event(new UserNotificationEvent($recipient, $sender, Notification::TYPE_MESSAGE_SENT, $message));

        $messages = $this->messageRepository->findAllByRecipientAndSender($message->recipient, $message->sender);

        $this->mailService->send(new EmailMessageMail($messages, $message->sender, $message->recipient));

        return $message;
    }
}

