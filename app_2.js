/**
 * We will use application as a global container of things and configs
 *
 * todo add DI for localStorage
 * todo find some way to make app accessible to all modules without global namespace usage
 * todo consider moving contents to main.js
 */
define([
    'helpers/AccessControl',
    'models/User',
    'js-cookie'
], function () {
    "use strict";

    var Application = {
        // The root path to run the application through.
        root: '/',

        // Store access control utility from dependencies
        accessControl: require('helpers/AccessControl'),

        // iOS app version
        iosAppVersion: '20.1.12',
        tempVersion: null,
        iosAppVersionIsValid: true,

        // Store user
        user: new (require('models/User'))({
            id: 'me',
            userType: 'guest'
        }),

        Cookies: require('js-cookie'),

        initSession: function () {
            if (!this.authTokenIsSet()) {
                return;
            }

            // todo Fetch user and store it
            this.user.fetch({
                async: false,
                success: _.bind(function (userModel) {
                    if (userModel.get('id')) {
                        $.ajaxSetup({
                            headers: {
                                'X-Auth': localStorage.getItem('AuthToken'),
                                'X-LearningSite-User': JSON.stringify({
                                    id: userModel.get('id'),
                                    email: userModel.get('email')
                                })
                            }
                        });

                        // Store AuthToken in user object
                        userModel.set('AuthToken', localStorage.getItem('AuthToken'));
                    }

                    // Validate ios app version
                    if ($.browser.cordova) {
                        var backendIosAppVersion = userModel.get('iosAppVersion');
                        if (!backendIosAppVersion) {
                            backendIosAppVersion = '0';
                        }
                        this.tempVersion = backendIosAppVersion;
                        this.iosAppVersionIsValid = this._iosAppVersionIsValid(backendIosAppVersion);
                    }

                    // Add sentry user context
                    if (typeof Raven !== "undefined") {
                        Raven.setUserContext({
                            id: userModel.get('id'),
                            email: userModel.get('email')
                        });
                    }
                }, this)
            });
            ///this.user.set('photoAsset', null); // wtf??

        },

        authTokenIsSet: function () {
            if (this.Cookies.get('AuthToken')) {
                localStorage.setItem('AuthToken', this.Cookies.get('AuthToken'));
                // Cookies with AuthToken expires in 60 seconds
                $.ajaxSetup({
                    headers: {
                        'X-Auth': localStorage.getItem('AuthToken')
                    }
                });

                this.deleteCookie('AuthToken', '/dashboard');
                return true;
            } else if (typeof imsToken !== 'undefined') {
                // Add IMS-Auth header to all ajax requests
                $.ajaxSetup({
                    headers: {
                        'IMS-Auth': imsToken
                    }
                });

                return true;
            } else if (localStorage.getItem('AuthToken')) {
                $.ajaxSetup({
                    headers: {
                        'X-Auth': localStorage.getItem('AuthToken')
                    }
                });

                return true;
            }

            return false;
        },

        applySession: function (session, remember) {
            localStorage.setItem('AuthToken', session.get('userToken'));
            this.initSession();

            if (!remember) {
                localStorage.removeItem('AuthToken');
            }
        },

        logout: function () {
            localStorage.removeItem('AuthToken');
            require('app').router.navigate('/');
            window.location.reload();
        },

        deleteCookie: function (name, path) {
            var domain = /:\/\/([^\/]+)/.exec(window.location.href)[1];
            this.Cookies.remove(name, {path: path, domain: domain});
        },

        _iosAppVersionIsValid: function (backendVersion) {

            backendVersion = parseInt(backendVersion.split('.').join(''));
            var frontendVersion = parseInt(this.iosAppVersion.split('.').join(''));

            return frontendVersion >= backendVersion;
        }
    };

    return Application;
});