$(document).ready(function() {
    var getAccountsURL = $('#todo-bar-add-input').attr('data-accounts');
    var getOppsURL = $('#todo-bar-add-input').attr('data-opps');

    $('#todo-bar-add-input').mentionsInput({
        triggerChar: "@",
        onDataRequest:function (mode, query, trigger, callback) {
            if(trigger == '@'){
                $.getJSON(getAccountsURL, {q: query}, function(response) {
                    var data =  response.data,
                        contactsEnabled = response.contactsEnabled;
                    data = _.filter(data, function(item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 });

                    callback.call(this, data);
                    if(!contactsEnabled) {
                        $('[data-uid="newcontact"]').hide();
                    }
                });
            }
            if(trigger == '#'){
                $.getJSON(getOppsURL, {q: query}, function(data) {
                    data = _.filter(data, function(item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 });

                    callback.call(this, data);
                });
            }
        }
    });

    // This event is called when we need to add a new account
    $(document).on("add-new-account-for-todo", "#todo-bar-add-input", function( event, searchQuery ) {
        event.stopPropagation();
        event.preventDefault();

        var leadRoute = Routing.generate('ajax_suggestion_add_lead');

        $("#addContactOrAccountForApportunityModal .modal-content").load(leadRoute, function() {
            $("#addContactOrAccountForApportunityModal").modal("show");
            $("#has_lead_add_new_lead_name").val(searchQuery);

        });
    });

    $(document).on('submit', '#newAjaxAddAccountForm', function(e){
        e.preventDefault();

        var form = $(this);
        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize()
        }).done(function (data) {
            var IS_JSON = (typeof(data) === 'object');
            if (IS_JSON) {
                if ($('#newOpportunityFromTodoModal').is(':visible')) {
                    // Update value in opportunity add modal
                    updateAutocompleteValueInTodoOpportunityModal(data[0], 'account');
                } else {
                    // Update value in todo bar
                    $( "#todo-bar-add-input").trigger( "add-new-account-for-todo-add-mention", [data] );
                }
                $('#addContactOrAccountForApportunityModal').modal('hide');
            } else {
                $(".modal-content", $('#addContactOrAccountForApportunityModal')).html(data);
            }
        }).fail(function (data) {

        });
    });

    // This event is called when we need to add a new contact
    $(document).on("add-new-contact-for-todo", "#todo-bar-add-input", function( event, searchQuery ) {
        event.stopPropagation();
        event.preventDefault();

        var contactRoute = Routing.generate('ajax_suggestion_add_contact');

        $("#addContactOrAccountForApportunityModal .modal-content").load(contactRoute, function() {
            $("#addContactOrAccountForApportunityModal").modal("show");
            $("#has_lead_contact_contact_name").val(searchQuery);

        });
    });

    $(document).on('submit', '#newAjaxAddContactForm', function(e){
        e.preventDefault();
        var form = $(this);
        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize()
        }).done(function (data) {
            var IS_JSON = (typeof(data) === 'object');
            if (IS_JSON) {
                if ($('#newOpportunityFromTodoModal').is(':visible')) {
                    updateAutocompleteValueInTodoOpportunityModal(data[0], 'contact');
                } else {
                    $( "#todo-bar-add-input").trigger( "add-new-contact-for-todo-add-mention", [data] );
                }
                $('#addContactOrAccountForApportunityModal').modal('hide');
            } else {
                $(".modal-content", $('#addContactOrAccountForApportunityModal')).html(data);
            }
        }).fail(function (data) {

        });
    });
});
