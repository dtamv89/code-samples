<?php

namespace TradingDeskApi\Managers;

use TradingDeskApi\Models\Constants\AdvertiserConstants;
use TradingDeskApi\Models\Constants\UserConstants;
use TradingDeskApi\Models\Advertiser;
use TradingDeskApi\Models\Vadn;
use TradingDeskApi\Models\SearchCriteria;
use Illuminate\Database\QueryException;
use TradingDeskApi\Exceptions\TradingDeskQueryParamException;

class AdvertiserManager extends AbstractManager
{
    /**
     * AdvertiserManager constructor.
     * @param Advertiser $advertiser
     */
    public function __construct(Advertiser $advertiser)
    {
        $this->model = $advertiser;
    }

    /**
     * @param SearchCriteria|null $searchCriteria
     * @param bool $getTotal
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * @throws TradingDeskQueryParamException
     */
    public function findAllWithProfiles(SearchCriteria $searchCriteria, $getTotal = false)
    {
        $query = $this->model->where('role', UserConstants::ROLE_ADVERTISER_SLUG);
        $query->leftJoin(AdvertiserConstants::ADVERTISER_PROFILES_TABLE, AdvertiserConstants::ADVERTISER_PROFILES_TABLE . '.user_id',
            '=', AdvertiserConstants::USERS_TABLE . '.id');

        $searchCriteria->buildQueryWithJoin($query, AdvertiserConstants::USERS_TABLE);

        if ($getTotal) {
            $searchCriteria->handleQueryForTotal($query);
        }

        try {
            $result = $query->get(AdvertiserConstants::FIELDS_TO_SELECT_FOR_PROFILE);
        } catch (QueryException $e) {
            throw new TradingDeskQueryParamException();
        }

        return $result;
    }

    /**
     * @param SearchCriteria|null $searchCriteria
     * @param Vadn $vadn
     * @param bool $getTotal
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     * @throws TradingDeskQueryParamException
     */
    public function findAllWithProfilesByVadn(SearchCriteria $searchCriteria, Vadn $vadn, $getTotal = false)
    {
        $query = $this->model->where('role', UserConstants::ROLE_ADVERTISER_SLUG);
        $query->where('vadn_id', $vadn->id);
        $query->leftJoin(AdvertiserConstants::ADVERTISER_PROFILES_TABLE, AdvertiserConstants::ADVERTISER_PROFILES_TABLE . '.user_id',
            '=', AdvertiserConstants::USERS_TABLE . '.id');

        $searchCriteria->buildQueryWithJoin($query, AdvertiserConstants::USERS_TABLE);

        if ($getTotal) {
            $searchCriteria->handleQueryForTotal($query);
        }

        try {
            $result = $query->get(AdvertiserConstants::FIELDS_TO_SELECT_FOR_PROFILE);
        } catch (QueryException $e) {
            throw new TradingDeskQueryParamException();
        }

        return $result;
    }

    /**
     * @param $bidder
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function findAllByBidder($bidder)
    {
        return $this->model->where("extension->mediation->bidder", $bidder)->get();
    }
}