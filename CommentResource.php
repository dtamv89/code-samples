<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use OpenApi\Annotations as OA;

class CommentResource extends JsonResource
{
    /**
     * @OA\Schema(
     *     schema="comment",
     *     title="Comment",
     *     type="object",
     *     required={"id", "user", "posts_id", "group_id", "message", "annotation_data",
     *               "annotation_number", "child_comments","created_at", "updated_at", "likes_count", "is_liked",},
     *     properties={
     *           @OA\Property(
     *                  property="id",
     *                  type="integer",
     *                  title="Post Id",
     *                  example=1,
     *              ),
     *              @OA\Property(
     *                  property="user",
     *                  type="object",
     *                  title="Author",
     *                  required={"id", "full_name", "username", "avatar_url"},
     *                  properties={
     *                      @OA\Property(
     *                          property="id",
     *                          title="user id",
     *                          type="integer",
     *                          example=1,
     *                      ),
     *                      @OA\Property(
     *                          property="full_name",
     *                          title="Full name",
     *                          type="string",
     *                          example="Full name",
     *                      ),
     *                      @OA\Property(
     *                          property="username",
     *                          title="Username",
     *                          type="string",
     *                          example="username",
     *                      ),
     *
     *                      @OA\Property(
     *                          property="avatar_url",
     *                          title="Avatar url",
     *                          type="string",
     *                          example=null,
     *                      )
     *                  }
     *              ),
     *              @OA\Property(
     *                  property="posts_id",
     *                  type="integer",
     *                  title="Post id",
     *                  example=1,
     *              ),
     *              @OA\Property(
     *                  property="group_id",
     *                  type="string",
     *                  title="Group id",
     *                  example="3cbd7b86-4282-4a1b-8740-1f0857d2bbe3",
     *              ),
     *              @OA\Property(
     *                  property="message",
     *                  type="string",
     *                  title="Message",
     *                  example="Comment",
     *              ),
     *              @OA\Property(
     *                  property="is_annotation",
     *                  type="booleant",
     *                  title="Is annotation",
     *                  example=false,
     *              ),
     *              @OA\Property(
     *                  property="annotation_data",
     *                  type="array",
     *                  description="Annotation data",
     *                  title="Annotation data",
     *                  @OA\Items()
     *              ),
     *              @OA\Property(
     *                  property="annotation_number",
     *                  type="integer",
     *                  title="Annotation number",
     *                  example=0,
     *              ),
     *              @OA\Property(
     *                  property="created_at",
     *                  type="string",
     *                  title="Created at",
     *                  format="ISO 8601",
     *                  example="2019-06-01T13:56:00+00:00",
     *              ),
     *              @OA\Property(
     *                  property="updated_at",
     *                  type="string",
     *                  title="Updated at",
     *                  format="ISO 8601",
     *                  example="2019-06-01T13:56:00+00:00",
     *              ),
     *              @OA\Property(
     *                  property="likes_count",
     *                  type="integer",
     *                  title="Likes count",
     *                  example=0,
     *              ),
     *              @OA\Property(
     *                  property="is_liked",
     *                  type="boolean",
     *                  title="Is post liked",
     *                  example=false,
     *              ),
     *              @OA\Property(
     *                  property="child_comments",
     *                  type="array",
     *                  title="Child comments",
     *                  @OA\Items(
     *                      ref="#/components/schemas/comment"
     *                  )
     *              ),
     *     }
     * )
     *
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => [
                'id' => $this->author->id,
                'username' => $this->author->username,
                'full_name' => $this->author->full_name ? $this->author->full_name : null,
                'avatar_url' => $this->author->avatar ? $this->author->avatar->url : null,
            ],
            'group_id' => $this->group_id,
            'post_id' => $this->post_id,
            'message' => $this->message,
            'is_annotation' => !$this->is_annotation ? false : true,
            'annotation_data' => $this->annotation_data,
            'annotation_number' => $this->annotation_number,
            'created_at' => Carbon::parse($this->created_at)->format("c"),
            'updated_at' => Carbon::parse($this->updated_at)->format("c"),
            'likes_count' => count($this->likes),
            'is_liked' => $request->user() ? $request->user()->isLiked($this->resource) : null,
            'child_comments' => CommentResource::collection($this->whenLoaded('childComments')),
        ];
    }
}

