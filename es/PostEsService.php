<?php

declare(strict_types=1);

namespace App\Services\Post;

use App\Models\Post;
use App\Services\ElasticSearchService;

class PostEsService
{
    public const POST_INDEX_NAME = 'posts';
    public const POST_TYPE = '_doc';

    private ElasticSearchService $elasticSearchService;


    public function __construct(ElasticSearchService $elasticSearchService)
    {
        $this->elasticSearchService = $elasticSearchService;
    }

    public function getPostsForMainPage(): array
    {
        $mobilePrototypesQuery = [
            'query' => [
                'bool' => [
                    'must' => [
                        ['terms' => ['content_type' => [Post::PROTOTYPE_CONTENT_TYPE, Post::EMBED_CODE_CONTENT_TYPE]]],
                        ['terms' => ['device_type' => [Post::MOBILE_IPHONE, Post::MOBILE_IPHONEX, Post::MOBILE_ANDROID]]],
                        ['term' => ['show_on_main' => true]]
                    ]
                ]
            ],
            'size' => self::MAIN_PAGE_MOBILE_POSTS_NUMBER,
            'sort' => ['rating' => ['order' => 'desc']],
        ];
        $desktopPrototypesQuery = [
            'query' => [
                'bool' => [
                    'must' => [
                        ['terms' => ['content_type' => [Post::PROTOTYPE_CONTENT_TYPE, Post::EMBED_CODE_CONTENT_TYPE]]],
                        ['terms' => ['device_type' => [Post::DESKTOP_TYPE]]],
                        ['term' => ['show_on_main' => true]]
                    ]
                ]
            ],
            'size' => self::MAIN_PAGE_DESKTOP_POSTS_NUMBER,
            'sort' => ['rating' => ['order' => 'desc']],
        ];
        $mobilePostsQuery = [
            'query' => [
                'bool' => [
                    'must' => [
                        ['terms' => ['content_type' => [Post::IMAGE_VIDEO_CONTENT_TYPE]]],
                        ['terms' => ['device_type' => [Post::MOBILE_IPHONE, Post::MOBILE_IPHONEX, Post::MOBILE_ANDROID]]],
                        ['term' => ['show_on_main' => true]]
                    ]
                ]
            ],
            'size' => self::MAIN_PAGE_MOBILE_POSTS_NUMBER,
            'sort' => ['rating' => ['order' => 'desc']],
        ];
        $desktopPostsQuery = [
            'query' => [
                'bool' => [
                    'must' => [
                        ['terms' => ['content_type' => [Post::IMAGE_VIDEO_CONTENT_TYPE, Post::A_B_CONTENT_TYPE]]],
                        ['terms' => ['device_type' => [Post::DESKTOP_TYPE]]],
                        ['term' => ['show_on_main' => true]]
                    ]
                ]
            ],
            'size' => self::MAIN_PAGE_DESKTOP_POSTS_NUMBER,
            'sort' => ['rating' => ['order' => 'desc']],
        ];

        // order must be preserved for returning data
        $body = [
            [(object) []],
            $mobilePrototypesQuery,
            ['index' => self::POST_INDEX_NAME],
            $desktopPrototypesQuery,
            ['index' => self::POST_INDEX_NAME],
            $mobilePostsQuery,
            ['index' => self::POST_INDEX_NAME],
            $desktopPostsQuery
        ];

        $data = [
            'index' => self::POST_INDEX_NAME,
            'body' => $body
        ];

        return $this->elasticSearchService->msearch($data);
    }
}

