<?php

namespace App\Http\Requests\Comment;

use App\Http\Requests\Request;
use OpenApi\Annotations as OA;

class CreateCommentRequest extends Request
{
    /**
     * @OA\RequestBody(
     *      request="create_comment",
     *      required=true,
     *      @OA\JsonContent(
     *          required={"post_id", "group_id", "is_annotation", "message"},
     *          @OA\Property(
     *              property="post_id",
     *              type="integer",
     *              description="Post id",
     *              title="Post id",
     *              example=1,
     *          ),
     *          @OA\Property(
     *              property="group_id",
     *              format="UUID",
     *              type="string",
     *              description="Uuid of Group. Generated on UI",
     *              title="File id",
     *              example="862e46e1-a635-4d38-b7f0-9d867bdc4619"
     *          ),
     *          @OA\Property(
     *              property="message",
     *              type="string",
     *              description="Message of comment",
     *              title="Message",
     *              example="Comment",
     *          ),
     *          @OA\Property(
     *              property="annotation_data",
     *              type="array",
     *              description="Annotation data",
     *              title="Annotation data",
     *              @OA\Items()
     *          ),
     *          @OA\Property(
     *              property="is_annotation",
     *              type="boolean",
     *              description="Is annotation type of comment",
     *              title="Is annotation",
     *              example=false
     *          ),
     *      )
     * )
     *
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'post_id' => 'required|integer|exists:posts,id',
            'group_id' => 'required|string|uuid',
            'is_annotation' => 'required|boolean',
            'annotation_data' => 'required_if:is_annotation,true|array',
            'message' => 'required|string|min:1',
        ];
    }
}

