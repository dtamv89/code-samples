<?php

namespace TradingDeskApi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use TradingDeskApi\Exceptions\TradingDeskAuthorizationException;
use TradingDeskApi\Exceptions\TradingDeskDoneAlreadyException;
use TradingDeskApi\Exceptions\TradingDeskNotFoundException;
use TradingDeskApi\Exceptions\TradingDeskQueryParamException;
use TradingDeskApi\Exceptions\TradingDeskValidationException;
use TradingDeskApi\Services\CampaignService;
use TradingDeskApi\Services\LineItemService;

class LineItemController extends Controller
{
    /**
     * @var LineItemService
     */
    protected $lineItemService;

    public function __construct(LineItemService $lineItemService)
    {
        $this->lineItemService = $lineItemService;
    }

    /**
     * Method retrieves Line Item by id
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Get(
     *     path="/v2/line-items/{id}",
     *     description="Returns Line Item",
     *     produces={"application/json"},
     *     tags={"Advertiser", "Admin", "VADN"},
     *     @SWG\Parameter(
     *       name="id",
     *       in="path",
     *       description="Line Item id",
     *       required=true,
     *       type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Line Item has been fetched",
     *         @SWG\Schema(ref="#/definitions/LineItem")
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Line Item not found"
     *     ),
     *     @SWG\Response(
     *         response=403,
     *         description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="Something went wrong"
     *     )
     * )
     *
     * @param $id
     */
    public function get($id)
    {
        try {
            $lineItem = $this->lineItemService->get($id);
        } catch (TradingDeskAuthorizationException $e) {
            return response()->json(['error' => 'Unauthorized'], Response::HTTP_FORBIDDEN);
        } catch (TradingDeskNotFoundException $e) {
            return response()->json(['error' => 'Line Item not found'], Response::HTTP_NOT_FOUND);
        }

        return response()->json($lineItem);
    }

    /**
     * Method retrieves list of Line Items
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Definition(
     *     definition="LineItemsForList",
     *     @SWG\Property(property="total", type="integer"),
     *     @SWG\Property(
     *             property="data",
     *             type="array",
     *             @SWG\Items(ref="#/definitions/LineItem")
     *         )
     * )
     *
     * @SWG\Get(
     *     path="/v2/line-items",
     *     description="Returns Line Items",
     *     produces={"application/json"},
     *     tags={"Advertiser", "Admin", "VADN"},
     *     @SWG\Parameter(
     *         name="limit",
     *         in="query",
     *         description="Limit",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="offset",
     *         in="query",
     *         description="Offset",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="filter_{field_name}",
     *         in="query",
     *         description="Property to filter by(e.g. ?filter_id=1,2,3,..,N)",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sort",
     *         in="query",
     *         description="Sort by field(e.g. ?sort=-id). Sort by field(e.g. ?sort=-id). Can be sorted by any field within LineItem model.
    Check LineItem model description to find out available fields.",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Line Items has been fetched",
     *         @SWG\Schema(ref="#/definitions/LineItemsForList")
     *     ),
     *      @SWG\Response(
     *         response=400,
     *         description="Invalid query parameter"
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="Something went wrong"
     *     )
     * )
     *
     * @param Request $request
     */
    public function list(Request $request)
    {
        $params = $request->query();

        try {
            $lineItems = $this->lineItemService->getAll($params);
        } catch (TradingDeskQueryParamException $e) {
            return response()->json(['errors' => $e->getErrors()], Response::HTTP_BAD_REQUEST);
        }

        return response()->json($lineItems);
    }

    /**
     * Method creates Line Item
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Post(
     *     path="/v2/line-items",
     *     description="Create new Line Item",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     tags={"Advertiser", "Admin", "VADN"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Line Item's data",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/LineItem")
     *     ),
     *     @SWG\Response(
     *         response=201,
     *         description="Line Item successfully created",
     *          @SWG\Schema(required={"id"},
     *          @SWG\Property(property="id", type="integer", format="int64")
     *       )
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Invalid data",
     *         @SWG\Schema(ref="#/definitions/ValidationError")
     *     ),
     *     @SWG\Response(
     *         response=403,
     *         description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="Something went wrong"
     *     )
     * )
     *
     * @param Request $request
     */
    public function create(Request $request)
    {
        $data = $request->all();

        try {
            $createdLineItemId = $this->lineItemService->create($data);
        } catch (TradingDeskValidationException $e) {
            return response()->json(['errors' => $e->getErrors()], Response::HTTP_BAD_REQUEST);
        } catch (TradingDeskAuthorizationException $e) {
            return response()->json(['error' => 'Unauthorized'], Response::HTTP_FORBIDDEN);
        }

        return response()->json(['id' => $createdLineItemId], Response::HTTP_CREATED);
    }

    /**
     * Method updates Line Item
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Put(
     *     path="/v2/line-items/{id}",
     *     description="Update Line Item",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     tags={"Advertiser", "Admin", "VADN"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Line Item id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         description="Line Item's data",
     *         required=true,
     *         @SWG\Schema(ref="#/definitions/LineItem")
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Line Item successfully updated"
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Invalid data",
     *         @SWG\Schema(ref="#/definitions/ValidationError")
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Line Item not found"
     *     ),
     *     @SWG\Response(
     *         response=403,
     *         description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="Something went wrong"
     *     )
     * )
     *
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();

        try {
            $this->lineItemService->update($data, $id);
        } catch (TradingDeskValidationException $e) {
            return response()->json(['errors' => $e->getErrors()], Response::HTTP_BAD_REQUEST);
        } catch (TradingDeskAuthorizationException $e) {
            return response()->json(['error' => 'Unauthorized'], Response::HTTP_FORBIDDEN);
        } catch (TradingDeskNotFoundException $e) {
            return response()->json(['error' => 'Line Item not found'], Response::HTTP_NOT_FOUND);
        }

        return response('');
    }

    /**
     * Method makes soft delete of Line Item
     *
     * @return mixed
     *
     * @SWG\Delete(
     *     path="/v2/line-items/{id}",
     *     description="Method makes soft delete of Line Item",
     *     produces={"application/json"},
     *     tags={"Advertiser", "Admin", "VADN"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Line Item id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=204,
     *         description="Line Item has been deleted"
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Invalid data",
     *         @SWG\Schema(ref="#/definitions/ValidationError")
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Line Item not found"
     *     ),
     *     @SWG\Response(
     *         response=403,
     *         description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="Something went wrong"
     *     )
     * )
     *
     * @param $id
     */
    public function archive($id)
    {
        try {
            $this->lineItemService->archive($id);
        } catch (TradingDeskAuthorizationException $e) {
            return response()->json(['error' => 'Unauthorized'], Response::HTTP_FORBIDDEN);
        } catch (TradingDeskNotFoundException $e) {
            return response()->json(['error' => 'Line Item not found'], Response::HTTP_NOT_FOUND);
        } catch (TradingDeskValidationException $e) {
            return response()->json(['errors' => $e->getErrors()], Response::HTTP_BAD_REQUEST);
        } catch(TradingDeskDoneAlreadyException $e){
            return response()->json(['general' => ['Done already']], Response::HTTP_OK);
        }

        return response()->json('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Method restores Line Item from soft deleted mode
     *
     * @return mixed
     *
     * @SWG\Put(
     *     path="/v2/line-items/{id}/unarchive",
     *     description="Method restores Line Item from soft deleted mode",
     *     produces={"application/json"},
     *     tags={"Advertiser", "Admin", "VADN"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="Line Item id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Line Item has been restored"
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Invalid data",
     *         @SWG\Schema(ref="#/definitions/ValidationError")
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Line Item not found"
     *     ),
     *     @SWG\Response(
     *         response=403,
     *         description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="Something went wrong"
     *     )
     * )
     *
     * @param $id
     */
    public function unarchive($id)
    {
        try {
            $this->lineItemService->unarchive($id);
        } catch (TradingDeskAuthorizationException $e) {
            return response()->json(['error' => 'Unauthorized'], Response::HTTP_FORBIDDEN);
        } catch (TradingDeskNotFoundException $e) {
            return response()->json(['error' => 'Line Item not found'], Response::HTTP_NOT_FOUND);
        } catch (TradingDeskValidationException $e) {
            return response()->json(['errors' => $e->getErrors()], Response::HTTP_BAD_REQUEST);
        } catch(TradingDeskDoneAlreadyException $e){
            return response()->json(['general' => ['Done already']], Response::HTTP_OK);
        }

        return response('');
    }

    /**
     * Method sets LineItem to pause mode
     *
     * @return mixed
     *
     * @SWG\Put(
     *     path="/v2/line-items/{id}/pause",
     *     description="Method set LineItem to pause mode",
     *     produces={"application/json"},
     *     tags={"Advertiser", "Admin", "VADN"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="LineItem's id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="LineItem has been set to pause mode"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="LineItem not found"
     *     ),
     *     @SWG\Response(
     *         response=403,
     *         description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Invalid data",
     *         @SWG\Schema(ref="#/definitions/ValidationError")
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="Something went wrong"
     *     )
     * )
     *
     * @param $id
     */
    public function pause($id)
    {
        try {
            $this->lineItemService->pause($id);
        } catch (TradingDeskAuthorizationException $e) {
            return response()->json(['error' => 'Unauthorized'], Response::HTTP_FORBIDDEN);
        } catch (TradingDeskNotFoundException $e) {
            return response()->json(['error' => 'LineItem not found'], Response::HTTP_NOT_FOUND);
        } catch (TradingDeskValidationException $e) {
            return response()->json(['errors' => $e->getErrors()], Response::HTTP_BAD_REQUEST);
        }

        return response('');
    }

    /**
     * Method restore LineItem from soft pause mode
     *
     * @return mixed
     *
     * @SWG\Put(
     *     path="/v2/line-items/{id}/approve",
     *     description="Method restore LineItem from pause mode",
     *     produces={"application/json"},
     *     tags={"Advertiser", "Admin", "VADN"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="LineItem's id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="LineItem has been restored from pause mode"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="LineItem not found"
     *     ),
     *     @SWG\Response(
     *         response=403,
     *         description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Invalid data",
     *         @SWG\Schema(ref="#/definitions/ValidationError")
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="Something went wrong"
     *     )
     * )
     *
     * @param $id
     */
    public function approve($id)
    {
        try {
            $this->lineItemService->approve($id);
        } catch (TradingDeskAuthorizationException $e) {
            return response()->json(['error' => 'Unauthorized'], Response::HTTP_FORBIDDEN);
        } catch (TradingDeskNotFoundException $e) {
            return response()->json(['error' => 'LineItem not found'], Response::HTTP_NOT_FOUND);
        } catch (TradingDeskValidationException $e) {
            return response()->json(['errors' => $e->getErrors()], Response::HTTP_BAD_REQUEST);
        }

        return response('');
    }

    /**
     * Method terminates Line Item
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Put(
     *     path="/v2/line-items/{id}/terminate",
     *     description="Method terminates LineItem",
     *     produces={"application/json"},
     *     tags={"Advertiser", "Admin", "VADN"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="LineItem's id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="LineItem has been terminated"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="LineItem not found"
     *     ),
     *     @SWG\Response(
     *         response=403,
     *         description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Invalid data",
     *         @SWG\Schema(ref="#/definitions/ValidationError")
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="Something went wrong"
     *     )
     * )
     *
     * @param $id
     */
    public function terminate($id)
    {
        try {
            $this->lineItemService->terminate($id);
        } catch (TradingDeskAuthorizationException $e) {
            return response()->json(['error' => 'Unauthorized'], Response::HTTP_FORBIDDEN);
        } catch (TradingDeskNotFoundException $e) {
            return response()->json(['error' => 'LineItem not found'], Response::HTTP_NOT_FOUND);
        } catch (TradingDeskValidationException $e) {
            return response()->json(['errors' => $e->getErrors()], Response::HTTP_BAD_REQUEST);
        } catch(TradingDeskDoneAlreadyException $e){
            return response()->json(['general' => ['Done already']], Response::HTTP_OK);
        }

        return response('');
    }

    /**
     * Method submits Line Item
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Put(
     *     path="/v2/line-items/{id}/submit",
     *     description="Method submits LineItem",
     *     produces={"application/json"},
     *     tags={"Advertiser", "Admin", "VADN"},
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         description="LineItem's id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Line Item has been submitted"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Line Item not found"
     *     ),
     *     @SWG\Response(
     *         response=403,
     *         description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Invalid data",
     *         @SWG\Schema(ref="#/definitions/ValidationError")
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="Something went wrong"
     *     )
     * )
     *
     * @param $id
     */
    public function submit($id)
    {
        try {
            $this->lineItemService->submit($id);
        } catch (TradingDeskAuthorizationException $e) {
            return response()->json(['error' => 'Unauthorized'], Response::HTTP_FORBIDDEN);
        } catch (TradingDeskValidationException $e) {
            return response()->json(['errors' => $e->getErrors()], Response::HTTP_BAD_REQUEST);
        } catch (TradingDeskNotFoundException $e) {
            return response()->json(['error' => 'Line Item not found'], Response::HTTP_NOT_FOUND);
        }

        return response('');
    }

    /**
     * Method transfers Line Item from Pending to Refused
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Put(
     *     path="/v2/line-items/{id}/refuse",
     *     description="Refuse Line Item",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     tags={"Advertiser", "Admin", "VADN"},
     *     @SWG\Parameter(
     *       name="id",
     *       in="path",
     *       description="Line Item id",
     *       required=true,
     *       type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Line Item successfully refused"
     *     ),
     *     @SWG\Response(
     *         response=403,
     *         description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Line Item not found"
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Invalid data",
     *         @SWG\Schema(ref="#/definitions/ValidationError")
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="Something went wrong"
     *     )
     * )
     *
     * @param $id
     */
    public function refuse($id)
    {
        try {
            $this->lineItemService->refuse($id);
        }  catch (TradingDeskAuthorizationException $e) {
            return response()->json(['error' => 'Unauthorized'], Response::HTTP_FORBIDDEN);
        } catch (TradingDeskValidationException $e) {
            return response()->json(['errors' => $e->getErrors()], Response::HTTP_BAD_REQUEST);
        } catch (TradingDeskNotFoundException $e) {
            return response()->json(['error' => 'Line Item not found'], Response::HTTP_NOT_FOUND);
        }

        return response('');
    }

    /**
     * Method transfers Line Item from Pending to Draft
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @SWG\Put(
     *     path="/v2/line-items/{id}/cancel",
     *     description="Cancel Line Item",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     tags={"Advertiser"},
     *     @SWG\Parameter(
     *       name="id",
     *       in="path",
     *       description="Line Item id",
     *       required=true,
     *       type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Line Item successfully canceled"
     *     ),
     *     @SWG\Response(
     *         response=403,
     *         description="Unauthorized"
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="Line Item not found"
     *     ),
     *     @SWG\Response(
     *         response=400,
     *         description="Invalid data",
     *         @SWG\Schema(ref="#/definitions/ValidationError")
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="Something went wrong"
     *     )
     * )
     *
     * @param $id
     */
    public function cancel($id)
    {
        try {
            $this->lineItemService->draft($id);
        }  catch (TradingDeskAuthorizationException $e) {
            return response()->json(['error' => 'Unauthorized'], Response::HTTP_FORBIDDEN);
        } catch (TradingDeskValidationException $e) {
            return response()->json(['errors' => $e->getErrors()], Response::HTTP_BAD_REQUEST);
        } catch (TradingDeskNotFoundException $e) {
            return response()->json(['error' => 'Line Item not found'], Response::HTTP_NOT_FOUND);
        }

        return response('');
    }
}